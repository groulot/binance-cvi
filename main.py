from __future__ import annotations
import atexit
import gc
import time
from datetime import timedelta
from decimal import Decimal
from functools import reduce
from threading import Thread
from typing import Optional, Tuple, List, Dict

import inject
import logging

import pandas
from binance import Client
from datetimeutc import Datetime
from kivy.app import App
from kivy.clock import mainthread
from kivy.properties import StringProperty, ObjectProperty, NumericProperty
from kivy.uix.tabbedpanel import TabbedPanel, TabbedPanelItem
from thespian.actors import ActorSystem, ActorAddress, ActorExitRequest, WakeupMessage
from kivy_garden.graph import Graph, MeshLinePlot

import bot_runner.config
import bot_runner.strategy.constant_value
from bot_runner import runner
from b.actor import DialoguingActor, Ask
from b.applog import MsgLog, MsgSetLogsDestination
from b.appstatus import MsgSetStatusDestination, MsgStatus
from b.credentials import Credentials
from b.exchange import Exchange
from b.message import DialogMessage, MsgInit
from b.phonebook_user import PhonebookUser
from b.spot.spot_market import Market
from b.spot.symbol import Symbol
from b.spot.account import Account

# Binance
from b.spot_utils import Position, position_from_order_list
from b.supervisor import Supervisor

api_key = ''
api_secret = ''

def get_account() -> Account:
    return Account()


def get_client() -> Client:
    return Client(api_key=api_key, api_secret=api_secret, testnet=True)


def get_credentials() -> Credentials:
    return Credentials(api_key, api_secret)


def get_exchange() -> Exchange:
    return Exchange(exchange="binance.com-test")


def get_logdef():
    return {'version': 1,
            'formatters': {
                'normal': {'format': '%(levelname)-8s %(message)s'},
                'actor': {'format': '%(levelname)-8s %(actorAddress)s %(actorName)s => %(message)s'}},
            'filters': {'isActorLog': {'()': ActorLogFilter},
                        'notActorLog': {'()': NotActorLogFilter}},
            'handlers': {'h1': {'class': 'logging.StreamHandler',
                                'formatter': 'normal',
                                'filters': ['notActorLog'],
                                'level': logging.WARNING,
                                'stream': 'ext://sys.stdout'},
                         'h2': {'class': 'logging.StreamHandler',
                                'formatter': 'actor',
                                'filters': ['isActorLog'],
                                'level': logging.INFO,
                                'stream': 'ext://sys.stdout'}},
            'loggers': {'': {'handlers': ['h1', 'h2'], 'level': logging.DEBUG}}
            }


def configure(binder):
    binder.bind_to_provider(Client, get_client)
    binder.bind_to_provider(Credentials, get_credentials)
    binder.bind_to_provider(Exchange, get_exchange)


class ActorLogFilter(logging.Filter):
    def filter(self, logrecord):
        d = logrecord.__dict__
        if 'actorAddress' in d:
            if 'actorName' not in d:
                d['actorName'] = "Unknown"
            return True
        return False


class NotActorLogFilter(logging.Filter):
    def filter(self, logrecord):
        return 'actorAddress' not in logrecord.__dict__


class BotInfo:
    def __init__(self, *, symbol: str):
        assert type(symbol) == str
        self.symbol = symbol
        self.last_order_time: int = 0
        self.last_kline_time: int = 0
        self.last_breakeven = None

class Main(App, Supervisor, PhonebookUser, DialoguingActor):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.bot_infos: List[Tuple[str, BotInfo]] = []
        self.python_main: Optional[ActorAddress] = None

    def build(self) -> UI:
        ui = UI()
        self.bind(on_start=self.kivy_init)
        return ui

    def kivy_init(self, _event):
        self.root_window.bind(on_request_close=self.close)

    def close(self, _event) -> bool:
        print("Stopping App")
        self.send(self.phonebook(), ActorExitRequest())
        self.shutting_down = True
        print(self.children)
        for _, child in list(self.children.items()):
            self.send(child.address, ActorExitRequest())
        time.sleep(5)
        self.send(self.python_main, "FINISHED")
        self.send(self.myAddress, ActorExitRequest())
        return False

    def child_recreated(self, child):
        pass

    class Run(DialogMessage):
        pass

    def _create_symbol(self, *, symbol: str, time_delta=timedelta(days=1)) -> ActorAddress:
        assert type(symbol) == str
        assert symbol.isupper()
        assert type(time_delta) == timedelta
        result = self.create_child(
            name='symbol_%s' % symbol.lower(),
            actor_class=Symbol,
            init_messages=[inject.instance(Client), symbol, time_delta, inject.instance(Exchange)])
        #result = self.createActor(Symbol)
        #self.send(result, inject.instance(Client))
        #self.send(result, symbol)
        #self.send(result, time_delta)
        #self.send(result, inject.instance(Exchange))
        yield from self.register(name='symbol_%s' % symbol.lower(), address=result)
        return result

    def _create_account(self) -> ActorAddress:
        account = self.create_child(
            name='account',
            actor_class=Account,
            init_messages=[inject.instance(Client), inject.instance(Credentials), inject.instance(Exchange)]
        )
        #account = self.createActor(Account)
        #self.send(account, inject.instance(Client))
        #self.send(account, inject.instance(Credentials))
        #self.send(account, inject.instance(Exchange))
        yield from self.register(name='account', address=account)
        return account

    def _create_market(self, *, symbol: str) -> ActorAddress:
        assert type(symbol) == str
        assert symbol.isupper()
        symbol_actor: ActorAddress = (yield from self.lookup(name='symbol_%s' % symbol.lower()))
        market = self.create_child(
            name='market_%s' % symbol.lower(),
            actor_class=Market,
            init_messages=[
                inject.instance(Client),
                inject.instance(Credentials),
                inject.instance(Exchange),
                symbol_actor]
        )
        #market = self.createActor(Market)
        #self.send(market, inject.instance(Client))
        #self.send(market, inject.instance(Credentials))
        #self.send(market, inject.instance(Exchange))
        #self.send(market, symbol_actor)
        yield Ask(recipient=market, message=MsgInit())
        yield from self.register(name='market_%s' % symbol.lower(), address=market)
        return market

    def _create_bot(self, *, symbol: str, fund: Decimal, worth_target: Decimal,
                    start_date: Datetime = None) -> ActorAddress:
        assert type(symbol) == str
        assert symbol.isupper()
        assert type(fund) == Decimal
        assert type(worth_target) == Decimal
        assert start_date is None or isinstance(start_date, Datetime)
        strategy = bot_runner.strategy.constant_value.ConstantValueStrategy(worth_target=worth_target, start_date=start_date)
        strategy_runner = self.create_child(
            name='%s bot' % symbol,
            actor_class=runner.StrategyRunner,
            init_messages=[
                bot_runner.config.Config(symbol_name=symbol, fund=fund, worth_target=worth_target),
                strategy
            ]
        )
        #strategy_runner = self.createActor(bot.StrategyRunner)
        #print("Sending bot config")
        #self.send(strategy_runner, bot.Config(
        #    symbol_name=symbol,
        #    fund=fund,
        #    worth_target=worth_target
        #))
        #strategy = bot.ConstantValueStrategy(worth_target=worth_target, start_date=start_date)
        #print("Sending strategy")
        #self.send(strategy_runner, strategy)
        yield Ask(recipient=strategy_runner, message=MsgSetLogsDestination(destination=self.myAddress))
        yield Ask(recipient=strategy_runner, message=MsgSetStatusDestination(destination=self.myAddress))
        print("Sending MsgInit")
        yield Ask(recipient=strategy_runner, message=MsgInit())
        self.send(strategy_runner, runner.StrategyRunner.Run())
        return strategy_runner

    def _run_bot(self, *, symbol: str, fund: Decimal, worth_target: Decimal, start_date: Datetime = None):
        assert type(symbol) == str and symbol.isupper()
        assert type(fund) == Decimal
        assert type(worth_target) == Decimal
        assert start_date is None or isinstance(start_date, Datetime)

        yield from self._create_symbol(symbol=symbol)
        yield from self._create_market(symbol=symbol)
        new_bot = yield from self._create_bot(
            symbol=symbol,
            fund=fund,
            worth_target=worth_target,
            start_date=start_date)
        self.bot_infos.append((new_bot, BotInfo(symbol=symbol)))

    def receiveMsg_Run(self, _message: Run, sender):
        self.python_main = sender
        inject.configure(configure)

        Thread(target=self.run).start()

        yield from self._create_account()

        self.bots = [
            self._run_bot(
                symbol='BNBUSDT',
                fund=Decimal(5000),
                worth_target=Decimal(2500),
        ]
        self.wakeupAfter(timePeriod=timedelta(seconds=1))

    def receiveMsg_WakeupMessage(self, message: WakeupMessage, sender):
        print("**********************\nRun new bot\n***********************")
        bot = self.bots.pop()
        yield from bot
        if len(self.bots) >0:
            self.wakeupAfter(timePeriod=timedelta(seconds=60))


    @mainthread
    def print_log(self, bot_info: BotInfo, text: str):
        if self.root is None:
            return

        ui: UI = self.root
        ui.add_symbol(bot_info.symbol)
        self.root.symbol_tabs[bot_info.symbol].log = text

    @mainthread
    def graph(self,
              *,
              bot_info: BotInfo,
              klines: List[Tuple[float, float]],
              breakevens: List[Tuple[float, float]],
              trades: List[Tuple[float, float]]):
        if self.root is None:
            return

        symbol = bot_info.symbol
        ui: UI = self.root
        ui.add_symbol(symbol)
        graph: SymbolGraph = self.root.symbol_tabs[symbol].graph
        for plot in graph.plots:
            graph.remove_plot(plot)

        kline_plot = MeshLinePlot(color=[1, 0, 0, 1])
        points = [(float(t // 1000), float(y)) for t, y in klines]
        indexmin = max(0, len(points) - graph.range)
        graph.xmin = points[indexmin][0]
        graph.xmax = points[-1][0]
        #graph.ymin = reduce(lambda m, t: min(m, t[1]), points[indexmin:], points[indexmin][1])
        #graph.ymax = reduce(lambda m, t: max(m, t[1]), points[indexmin:], points[indexmin][1])
        kline_plot.points = points
        graph.add_plot(kline_plot)

        trade_plot = MeshLinePlot(color=[0, 1, 0, 1])
        points = [(float(t // 1000), float(y)) for t, y in trades]
        trade_plot.points = points
        graph.add_plot(trade_plot)

        be_plot = MeshLinePlot(color=[1, 0.5, 0.5, 1])
        points = [(float(t // 1000), float(y)) for t, y in breakevens]
        be_plot.points = points
        graph.add_plot(be_plot)

    def _search_bot_info(self, sender: ActorAddress) -> Optional[BotInfo]:
        for t in self.bot_infos:
            if t[0] == sender:
                return t[1]
        return None

    def receiveMsg_MsgLog(self, message: MsgLog, sender: ActorAddress):
        bot_info = self._search_bot_info(sender)
        if bot_info is not None:
            self.print_log(bot_info, message.message)
        else:
            print(message.message)

    def receiveMsg_MsgStatus(self, status: MsgStatus, sender: ActorAddress):
        bot_info = self._search_bot_info(sender)
        if bot_info is not None:
            strategy_status: bot_runner.strategy.constant_value.ConstantValueStrategy.Status = status.status
            last_kline = strategy_status.klines.iloc[-1]
            last_order = strategy_status.orders.iloc[-1]
            #print("%s < %s ?" % (bot_info.last_order_time, last_order.time))
            if bot_info.last_order_time < last_order.time or bot_info.last_kline_time < last_kline.open_time:
                #print(last_kline)
                bot_info.last_kline_time = last_kline.open_time
                # Limit to 4000 points
                data = strategy_status.klines
                data['open_time'] = pandas.to_datetime(data['open_time'], unit='ms')
                data = data.set_index('open_time', drop=False)
                if len(data) > 4000:
                    group_by = len(data) // 2000
                    data = data.resample('%sMin' % group_by).mean().reset_index().set_index('open_time', drop=False)
                data['open_time'] = (data['open_time'] - pandas.Timestamp("1970-01-01")) // pandas.Timedelta('1s')
                klines = [tuple(r) for r in data.to_numpy()]
                trades = [(o[1].time, o[1].cummulativeQuoteQty / o[1].executedQty) for o in strategy_status.orders.iterrows()]

                if bot_info.last_order_time < last_order.time:
                    print("New order to graph")
                    orders = strategy_status.orders
                    bot_info.last_order_time = orders.iloc[-1].time
                    positions: List[Position] = []
                    for index, _ in orders.iterrows():
                        positions.append(position_from_order_list(orders.loc[:index]))
                    breakevens = [(p.date.timestamp(), p.price) for p in positions]
                    bot_info.last_breakeven = breakevens
                    print("Last break even for %s: %s" % (bot_info.symbol, breakevens[-1][1]))
                else:
                    breakevens = bot_info.last_breakeven

                self.graph(bot_info=bot_info, klines=klines, breakevens=breakevens, trades=trades)


class UI(TabbedPanel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.symbol_tabs: Dict[str, SymbolTab] = {}

    def add_symbol(self, name):
        if name not in self.symbol_tabs:
            print("Adding symbol tab for %s" % name)
            tab = SymbolTab(symbol=name, log="")
            self.symbol_tabs[name] = tab
            self.add_widget(tab)


class SymbolTab(TabbedPanelItem):
    symbol = StringProperty()
    log = StringProperty()
    graph = ObjectProperty()

class SymbolGraph(Graph):
    range = NumericProperty(1000)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.minmaxes: Dict[Tuple[float, float]] = {}

    def add_plot(self, plot: MeshLinePlot):
        # search for xmin corresponding index
        try:
            start_point = next(point for point in plot.points if point[0] >= self.xmin and point[0] <= self.xmax)
            #print("Start point: %s, %s" % start_point)
        except StopIteration:
            start_point = None
        if start_point is not None:
            start_index = plot.points.index(start_point)
            #print("Start index: %s" % start_index)
            ymin = reduce(lambda m, t: min(m, t[1]), plot.points[start_index:], plot.points[start_index][1])
            ymax = reduce(lambda m, t: max(m, t[1]), plot.points[start_index:], plot.points[start_index][1])
            #print("ymin: %s ymax: %s" % (ymin, ymax))
            self.minmaxes[plot] = (ymin, ymax)
            self.set_y_minmax()
            super().add_plot(plot)

    def remove_plot(self, plot):
        del self.minmaxes[plot]
        super().remove_plot(plot)

    def set_y_minmax(self):
        #print("Graph: %s" % self)
        #print(self.minmaxes)
        if len(self.minmaxes) > 0:
            first = list(self.minmaxes.values())[0]
            self.ymin = reduce(lambda a, b: min(a, b[0]), self.minmaxes.values(), first[0])
            self.ymax = reduce(lambda a, b: max(a, b[1]), self.minmaxes.values(), first[1])
            #print("All plots ymin: %s ymax: %s" % (self.ymin, self.ymax))
            self.y_ticks_major = (self.ymax - self.ymin) / 10
            self.y_grid_label = True

if __name__ == "__main__":
    asys = ActorSystem(systemBase='multiprocQueueBase', logDefs=get_logdef())
    #asys = ActorSystem(systemBase='simpleSystemBase', logDefs=get_logdef())
    #asys = ActorSystem(systemBase='simpleSystemBase', logDefs=get_logdef())
    atexit.register(lambda: asys.shutdown())

    time.sleep(5)
    main = asys.createActor(Main)
    asys.ask(main, Main.Run())
    asys.shutdown()
