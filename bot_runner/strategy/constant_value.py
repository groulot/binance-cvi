from datetime import timedelta
from decimal import Decimal
from typing import Optional, Coroutine, Mapping, Type, List

from binance import Client
from datetimeutc import Datetime
from pandas import DataFrame, Series

from b.appstatus import Status
from b.spot.account import Balance
from b.spot.symbol_filter import SymbolFilter
from b.spot_utils import drop_obsolete_orders, get_first_position, position_from_order_list
from bot_runner.strategy.strategy import Strategy


class TraillingStop:
    def __init__(self, *, price: Decimal, percent: Decimal = Decimal('1.5'), trailling_percent: Decimal = Decimal(10)):
        assert type(price) == Decimal
        self.start_price: Decimal = price
        assert type(percent) == Decimal
        self.percent = percent / Decimal(100)
        assert type(trailling_percent) == Decimal
        self.trailling_percent = trailling_percent / Decimal(100)
        self.max: Decimal = price
        self.min: Decimal = price
        self.prices: List[Decimal] = []

    def need_rebalance(self, *, price: Decimal) -> bool:
        assert type(price) == Decimal
        self.prices.append(price)
        if len(self.prices) > 30:
            self.prices = self.prices[-30:]
        smooth_price = sum(self.prices) / len(self.prices)
        return self.need_rebalance2(price=smooth_price) and self.need_rebalance2(price=price)

    def need_rebalance2(self, *, price: Decimal) -> bool:
        self.max = max(self.max, price)
        self.min = min(self.min, price)
        if price / self.start_price < 1 + self.percent and price / self.start_price > 1 - self.percent:
            return False
        if price > self.start_price and price < self.max - (self.max - self.start_price) * self.trailling_percent:
            return True
        if price < self.start_price and price > self.min + (self.start_price - self.min) * self.trailling_percent:
            return True
        return False

class ConstantValueStrategy(Strategy):
    class Status(Status):
        def __init__(self,
                     *,
                     trailling_stop: TraillingStop,
                     coin: Decimal,
                     fund: Decimal,
                     price: Decimal,
                     initial_price: Decimal,
                     initial_date: Datetime,
                     initial_coin: Decimal,
                     breakeven: Decimal,
                     rebalances: int,
                     klines: DataFrame,
                     orders: DataFrame
                     ):
            assert type(trailling_stop) == TraillingStop
            self.trailling_stop = trailling_stop
            assert type(coin) == Decimal
            self.coin = coin
            assert type(fund) == Decimal
            self.fund = fund
            assert type(price) == Decimal
            self.price = price
            assert type(initial_price) == Decimal
            self.initial_price = initial_price
            assert isinstance(initial_date, Datetime)
            self.initial_date = initial_date
            assert type(initial_coin) == Decimal
            self.initial_coin = initial_coin
            assert type(breakeven) == Decimal
            self.breakeven = breakeven
            assert type(rebalances) == int
            self.rebalances = rebalances
            assert type(klines) == DataFrame
            self.klines = klines
            assert type(orders) == DataFrame
            self.orders = orders

        def __str__(self):
            msg = ["First buy at {} with price {:8.8f}".format(self.initial_date, self.initial_price),
                   "Coin worth: {:8.8f}".format(self.coin * self.price),
                   "Total worth: {:8.8f}".format(self.coin * self.price + self.fund),
                   "Breakeven: {:8.8f}".format(self.breakeven)]
            hworth = self.price * self.initial_coin * 2
            if hworth != Decimal(0):
                msg.append("Comparative HODL worth: {:8.8f} ({:3.4f}%)".format(
                    hworth,
                    Decimal(100) * (self.coin * self.price + self.fund) / hworth))
            msg += ["Coins: {:8.8f}".format(self.coin),
                    "Funds: {:8.8f}".format(self.fund),
                    "Rebalances: {}".format(self.rebalances),
                    "Money if back to first price : {:8.8f}".format(
                        (self.coin * self.initial_price + self.fund))]
            return "\n".join(msg)

    def __init__(self, *, worth_target: Decimal, start_date: Optional[Datetime] = None):
        super().__init__()
        assert type(worth_target) == Decimal
        self.worth_target = worth_target
        assert start_date is None or isinstance(start_date, Datetime)
        self.start_date = Datetime(year=2001, month=1, day=1) if start_date is None else start_date

        self.display = False
        self.rebalance_percent_threshold: Decimal = Decimal('0.005')
        self.rebalance_min_quote_amount: Decimal = Decimal(20)
        self.klines: DataFrame = DataFrame(columns=['open_time', 'close']) \
            .set_index('open_time', drop=False) \
            .astype(dtype=float)

    def log(self, message):
        yield Strategy.Log(message=message)

    def status(self, status):
        yield Strategy.Status(status=status)

    def run(self) -> Coroutine[Strategy.Request, Series, None]:
        # Ask for provided funds
        fund = yield Strategy.AskFund()
        assert type(fund) == Decimal
        self.start_fund: Decimal = fund
        self.fund: Decimal = fund

        # Ask for broker fee
        fee = yield Strategy.AskFee()
        assert type(fee) == Decimal
        self.fee = fee

        # Ask for balance
        balance = yield Strategy.AskBalance()
        assert type(balance) == Balance
        self.coin: Decimal = balance.free
        self.nb_rebalances: int = 0

        # Ask for order history
        self.order_history = yield Strategy.AskOrderHistory(start_date=self.start_date)
        self.order_history = self.order_history.query("status == 'FILLED'")
        self.order_history = drop_obsolete_orders(self.order_history)

        # Adjust start date
        if len(self.order_history) > 0:
            self.start_date = Datetime.fromtimestamp(self.order_history.iloc[0].time / 1000)
        else:
            # By default, one day
            self.start_date = Datetime.now() - timedelta(days=1)

        # Ask for Klines
        kline_history = yield Strategy.AskKlineHistory(start_date=self.start_date, columns=['open_time', 'close'])
        self.klines = self.klines.append(kline_history)

        # Get starting position
        position = get_first_position(orders=self.order_history, worth_target=self.worth_target)

        if position is None:
            position = position_from_order_list(orders=self.order_history)

        self.initial_breakeven_price: Decimal = position.price
        self.initial_breakeven_coin: Decimal = position.quantity
        self.initial_time: Datetime = position.date
        self.initial_fund: Decimal = self.fund - self.initial_breakeven_price * self.initial_breakeven_coin

        # Get current position
        position = position_from_order_list(orders=self.order_history)
        self.fund -= position.price * position.quantity
        self.breakeven = position.price

        symbol_filter = yield Strategy.AskSymbolFilter()
        assert type(symbol_filter) == SymbolFilter
        self.symbol_filter: SymbolFilter = symbol_filter
        #self.sma9: float = 0.0
        #self.sma20: float = 0.0
        self.kline: Optional[Series] = None
        self.last_summary = Datetime.now() - timedelta(days=1)
        self.response = yield Strategy.AskKLine()

        # Configure trailling stop with last trade's price
        if len(self.order_history) > 0:
            last_order = self.order_history.iloc[-1]
            last_price = Decimal(last_order['cummulativeQuoteQty']) / Decimal(last_order['executedQty'])
            self.trailling_stop = TraillingStop(price=last_price)
        else:
            self.trailling_stop = TraillingStop(price=Decimal(self.response.kline.close))
        yield from self.log(message="Trailling stop set at: {:8.8f}".format(self.trailling_stop.start_price))


        self.waiting_on_order = False
        while True:
            if type(self.response) == Strategy.KLine:
                yield from self.on_kline(self.response.kline)
            # if type(self.response) == Strategy.NewBalances:
            #    new_balance: Strategy.NewBalances = self.response
            #    self.coin = new_balance.base_asset
            #    self.fund = new_balance.quote_asset
            #    yield from self._summary(self.kline)
            #    self.last_summary = Datetime.now()
            if type(self.response) == Strategy.OrderStatus:
                order_status: Strategy.OrderStatus = self.response
                yield from self.log("Order status received: %s" % order_status)
                if order_status.status == Client.ORDER_STATUS_FILLED:
                    self.waiting_on_order = False
                    order = {
                        'cummulativeQuoteQty': order_status.cumulativeQuoteAssetTransactedQty,
                        'executedQty': order_status.cumulativeFilledQty,
                        'side': order_status.side,
                        'type': order_status.type,
                        'status': order_status.status,
                        'time': int(order_status.date.timestamp() * 1000)
                    }
                    self.order_history =        self.order_history.append(Series(data=order, name=order['time']))
                    self.breakeven = position_from_order_list(orders=self.order_history).price
                    if order_status.side == Client.SIDE_SELL:
                        self.coin -= order_status.cumulativeFilledQty
                        self.fund += order_status.cumulativeQuoteAssetTransactedQty
                    elif order_status.side == Client.SIDE_BUY:
                        self.coin += order_status.cumulativeFilledQty
                        self.fund -= order_status.cumulativeQuoteAssetTransactedQty
                    yield from self._summary(self.kline)

            self.response = yield Strategy.AskKLine()

    def worth(self, price: Decimal):
        return self.coin * price

    def _rebalance(self, *, worth_diff: Decimal, kline: Series):
        close = Decimal(kline.close)
        self.nb_rebalances += 1
        if worth_diff > Decimal(0):
            # Need to sell
            self.waiting_on_order = True
            if self.display:
                print("Selling base asset around price {:8.8f} for {:8.8f} quote asset".format(close, worth_diff))
                yield from self.log("Selling base asset around price {:8.8f} for {:8.8f} quote asset".format(
                    close,
                    worth_diff
                ))
            self.response = yield Strategy.PlaceOrder(
                side=Client.SIDE_SELL,
                quote_value=self.symbol_filter.filter_quote_order_quantity(quantity=worth_diff))
        elif worth_diff <= Decimal(0) and self.fund > abs(worth_diff):
            # Need to buy
            print("Need to buy")
            self.waiting_on_order = True
            if self.display:
                print("Buying base asset around price {:8.8f} for {:8.8f} quote asset".format(close, -worth_diff))
                yield from self.log("Buying base asset around price {:8.8f} for {:8.8f} quote asset".format(
                    close,
                    -worth_diff
                ))
            self.response = yield Strategy.PlaceOrder(
                side=Client.SIDE_BUY,
                quote_value=self.symbol_filter.filter_quote_order_quantity(quantity=-worth_diff))
            if self.initial_breakeven_price == Decimal(0):
                self.initial_breakeven_price = close

    def _status(self, kline: Series):
        return ConstantValueStrategy.Status(
            trailling_stop=self.trailling_stop,
            coin=self.coin,
            fund=self.fund,
            price=Decimal(kline.close),
            initial_price=self.initial_breakeven_price,
            initial_date=self.initial_time,
            initial_coin=self.initial_breakeven_coin,
            breakeven=self.breakeven,
            rebalances=self.nb_rebalances,
            klines=self.klines,
            orders=self.order_history
        )

    def _summary(self, kline: Series):
        if not self.display:
            return
        status = self._status(kline)
        self.last_summary = Datetime.now()
        yield from self.log(message=str(status))

    def on_kline(self, kline: Series):
        self.klines.loc[kline.name] = kline
        if self.last_summary < Datetime.now() - timedelta(days=1):
            yield from self._summary(kline)
        # logging.info("kline: %s" % kline.close)
        self.kline = kline
        close = Decimal(kline.close)
        worth = self.worth(close)
        worth_diff = worth - self.worth_target
        if self.initial_breakeven_price == Decimal(0):
            yield from self._rebalance(worth_diff=worth_diff, kline=kline)
            yield from self._summary(kline)
            return
        # if ((kline.SMA9 < kline.SMA20 and self.sma9 > self.sma20)
        #    or (kline.SMA9 > kline.SMA20 and self.sma9 < self.sma20)
        #    ) and abs(worth_diff) > self.worth_target * Decimal('0.02') and not self.waiting_on_order \
        #        and abs(worth_diff) > self.symbol_filter.min_notional:
        #    print('SMA crossing')
        #    yield from self._rebalance(worth_diff=worth_diff, kline=kline)
        # if not self.waiting_on_order and abs(worth_diff) > self.worth_target * Decimal('0.05') \
        #        and abs(worth_diff) > self.symbol_filter.min_notional:
        #    print('Worth diff > 5%')
        #    yield from self._rebalance(worth_diff=worth_diff, kline=kline)
        if self.trailling_stop.need_rebalance(price=close) and abs(worth_diff) > self.symbol_filter.min_notional:
            self.trailling_stop = TraillingStop(price=close)
            yield from self._rebalance(worth_diff=worth_diff, kline=kline)
        # self.sma9 = kline.SMA9
        # self.sma20 = kline.SMA20
        status = self._status(kline)
        yield from self.status(status=status)

    def get_performance(self) -> Mapping[Type[Strategy.PerformanceType], Strategy.PerformanceType]:
        holdWorth = self.start_fund * Decimal(self.kline.close) / self.initial_breakeven_price
        ourWorth = self.worth(Decimal(self.kline.close)) + self.fund
        ourWorthStartPrice = self.worth((self.initial_breakeven_price)) + self.fund
        print("Number of rebalances: %s" % self.nb_rebalances)
        print("Start fund: %s" % self.start_fund)
        print("Initial breakeven price: %s" % self.initial_breakeven_price)
        print("Last price: %s" % self.kline.close)
        print("Last fund: %s" % self.fund)
        print("Last coin worth: %s" % self.worth(Decimal(self.kline.close)))
        return {
            Strategy.PerformanceTypeVsNothing: Strategy.PerformanceTypeVsNothing(ourWorth / self.start_fund),
            Strategy.PerformanceTypeVsHold: Strategy.PerformanceTypeVsHold(ourWorth / holdWorth),
            Strategy.PerformanceTypeAfterSwing: Strategy.PerformanceTypeAfterSwing(ourWorthStartPrice / self.start_fund)
        }
