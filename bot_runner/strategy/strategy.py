from abc import ABC, abstractmethod
from decimal import Decimal
from typing import Mapping

from binance import Client
from datetimeutc import Datetime
from pandas import Series

from b.appstatus import Status

python_type = type

class Strategy(ABC):
    class PerformanceType(ABC):
        def __init__(self, perf: Decimal):
            assert type(perf) == Decimal
            self.perf = perf * Decimal('100')

        @abstractmethod
        def __str__(self):
            pass

    class PerformanceTypeVsNothing(PerformanceType):
        def __str__(self):
            return "VS no action: {:.2f}%".format(self.perf)

    class PerformanceTypeVsHold(PerformanceType):
        def __str__(self):
            return "VS Hold: {:.2f}%".format(self.perf)

    class PerformanceTypeAfterSwing(PerformanceType):
        def __str__(self):
            return "VS swing to same price: {:.2f}%".format(self.perf)

    class Request:
        pass

    class AskKLine(Request):
        pass

    class AskFund(Request):
        pass

    class AskFee(Request):
        pass

    class AskBalance(Request):
        pass

    class AskOrderHistory(Request):
        def __init__(self, *, start_date: Datetime):
            assert isinstance(start_date, Datetime)
            self.start_date = start_date

    class AskKlineHistory(Request):
        def __init__(self, *, start_date: Datetime, columns=None):
            assert isinstance(start_date, Datetime)
            self.start_date = start_date
            self.columns = columns

    class AskSymbolFilter(Request):
        pass

    class PlaceOrder(Request):
        def __init__(self, *, side, quote_value):
            assert side in [Client.SIDE_SELL, Client.SIDE_BUY]
            self.side = side
            assert type(quote_value) == Decimal and quote_value > Decimal(0)
            self.quote_value = quote_value

    class Log(Request):
        def __init__(self, message):
            assert type(message).__str__ is not object.__str__
            self.message = message

    class Status(Request):
        def __init__(self, status):
            assert isinstance(status, Status)
            self.status = status

    class Response:
        pass

    class KLine(Response):
        def __init__(self, *, kline: Series):
            assert type(kline) == Series
            self.kline = kline


    class OrderStatus(Response):
        def __init__(self,
                     *,
                     side: str,
                     type: str,
                     clientOrderId: str,
                     status: str,
                     cumulativeFilledQty: Decimal,
                     cumulativeQuoteAssetTransactedQty: Decimal,
                     date: Datetime):
            assert side in [Client.SIDE_SELL, Client.SIDE_BUY]
            self.side = side
            assert type in [Client.ORDER_TYPE_LIMIT,
                            Client.ORDER_TYPE_MARKET,
                            Client.ORDER_TYPE_STOP_LOSS,
                            Client.ORDER_TYPE_STOP_LOSS_LIMIT,
                            Client.ORDER_TYPE_TAKE_PROFIT,
                            Client.ORDER_TYPE_TAKE_PROFIT_LIMIT,
                            Client.ORDER_TYPE_LIMIT_MAKER]
            self.type = type
            assert python_type(clientOrderId) == str
            self.clientOrderId = clientOrderId
            assert status in [
                Client.ORDER_STATUS_NEW,
                Client.ORDER_STATUS_PARTIALLY_FILLED,
                Client.ORDER_STATUS_FILLED,
                Client.ORDER_STATUS_CANCELED,
                Client.ORDER_STATUS_PENDING_CANCEL,
                Client.ORDER_STATUS_REJECTED,
                Client.ORDER_STATUS_EXPIRED]
            self.status = status
            assert python_type(cumulativeFilledQty) == Decimal
            self.cumulativeFilledQty = cumulativeFilledQty
            assert python_type(cumulativeQuoteAssetTransactedQty) == Decimal
            self.cumulativeQuoteAssetTransactedQty = cumulativeQuoteAssetTransactedQty
            assert isinstance(date, Datetime)
            self.date = date

    @abstractmethod
    def run(self):
        pass

    @abstractmethod
    def get_performance(self) -> Mapping[PerformanceType, Decimal]:
        pass