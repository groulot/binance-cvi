import logging
from decimal import Decimal

from thespian.actors import ActorAddress
from thespian.initmsgs import initializing_messages

from b.actor import DialoguingActor, Ask
from b.message import MsgInit, Acknowledge
from b.phonebook_user import PhonebookUser
from b.spot.symbol import MsgGetBaseAsset, MsgGetQuoteAsset, MsgGetSymbolFilter
from bot_runner.config import Config
from bot_runner.strategy.strategy import Strategy


@initializing_messages([('config', Config), ('strategy', Strategy)])
class StrategyRunnerBase(PhonebookUser, DialoguingActor):
    def receiveMsg_MsgInit(self, _message: MsgInit, sender):
        yield from self.do_init()
        self.reply(sender, Acknowledge())

    def do_init(self):
        self.symbol_name: str = self.config.symbol

        old_factory = logging.getLogRecordFactory()

        def record_factory(*args, **kwargs):
            record = old_factory(*args, **kwargs)
            record.actorName = self.symbol_name
            return record

        logging.setLogRecordFactory(record_factory)

        self.fund: Decimal = self.config.fund
        self.fee: Decimal = self.config.fee
        # lookup the symbol's actor
        self.symbol: ActorAddress = yield from self.lookup(name='symbol_{}'.format(self.symbol_name.lower()))

        response2 = yield Ask(recipient=self.symbol, message=MsgGetBaseAsset())
        self.base_asset = response2.base_asset
        response3 = yield Ask(recipient=self.symbol, message=MsgGetQuoteAsset())
        self.quote_asset = response3.quote_asset
        response4 = yield Ask(
            recipient=self.symbol,
            message=MsgGetSymbolFilter(symbol=self.symbol_name))
        self.symbol_filter = response4.symbol_filter