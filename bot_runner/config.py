from decimal import Decimal


class Config:
    def __init__(self,
                 *,
                 symbol_name: str,
                 fund: Decimal,
                 worth_target: Decimal,
                 fee: Decimal = Decimal('0.00075')):
        assert type(symbol_name) == str
        self.symbol = symbol_name
        assert type(fund) == Decimal and fund >= Decimal(0)
        self.fund = fund
        assert type(worth_target) == Decimal
        self.worth_target = worth_target
        assert type(fee) == Decimal and fee >= Decimal(0)
        self.fee = fee