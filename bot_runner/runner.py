from __future__ import annotations

import gc
import tracemalloc
from decimal import Decimal

import setproctitle
from binance import Client
from thespian.actors import ActorAddress
from to_precision import to_precision

from b.actor import Ask
from b.applog import AppLog
from b.appstatus import AppStatus
from b.message import Response, DialogMessage
from b.spot.market import MsgPlaceOrderResponse, MsgPlaceOrder, MsgExecutionReport
from b.spot.account import MsgGetOrderHistory, MsgGetBalance
from b.spot.symbol import MsgKLine, MsgSubscribeKLines, MsgGetKLines
from b.spot_utils import display_top
from bot_runner.base import StrategyRunnerBase
from bot_runner.strategy.strategy import Strategy

python_type = type


def _format_price(f):
    return to_precision(f, 6, 'std', strip_zeros=True, preserve_integer=True)


class StrategyRunner(AppStatus, AppLog, StrategyRunnerBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    class Run(DialogMessage):
        pass

    def log(self, message: str):
        message = "%s bot: %s" % (self.symbol_name, message)
        super().log(message)

    def do_init(self):
        yield from super().do_init()
        self.log("Initialising...")
        self.account: ActorAddress = yield from self.lookup(name='account')
        self.market: ActorAddress = yield from self.lookup(name='market_%s' % self.symbol_name.lower())

        response = yield Ask(recipient=self.account, message=MsgGetBalance(asset_name=self.base_asset))
        assert type(response) == Response and response.balance is not None
        self.balance = response.balance

        # Set up strategy coroutine
        self.strategy.display = True
        self.coro = self.strategy.run()
        self.action = None
        self.last_kline = None
        gc.collect()

    def receiveMsg_Run(self, _message: Run, _sender):
        self.log("Starting...")
        # Subscribe to KLines
        self.send(self.symbol, MsgSubscribeKLines())
        # Run strategy
        yield from self.run_strategy()
        setproctitle.setproctitle("Thespian: %s bot" % self.symbol_name)
        if tracemalloc.is_tracing():
            display_top(tracemalloc.take_snapshot())

    def run_strategy(self, reply=None):
        self.action = self.coro.send(reply)
        # Process and get desired action, give reply to previous action too
        if type(self.action) == Strategy.AskFund:
            yield from self.run_strategy(self.fund)
        if type(self.action) == Strategy.AskFee:
            yield from self.run_strategy(self.fee)
        if type(self.action) == Strategy.AskBalance:
            yield from self.run_strategy(self.balance)
        if type(self.action) == Strategy.AskOrderHistory:
            responce = yield Ask(
                recipient=self.account,
                message=MsgGetOrderHistory(symbol=self.symbol_name, start_date=self.action.start_date))
            assert type(responce) == Response
            yield from self.run_strategy(responce.orders)
        if type(self.action) == Strategy.AskKlineHistory:
            responce = yield Ask(
                recipient=self.symbol,
                message=MsgGetKLines(
                    start_datetime=self.action.start_date,
                    columns=self.action.columns))
            assert type(responce) == Response
            yield from self.run_strategy(responce.klines)
        if type(self.action) == Strategy.AskSymbolFilter:
            yield from self.run_strategy(self.symbol_filter)
        if type(self.action) == Strategy.PlaceOrder:
            order: Strategy.PlaceOrder = self.action
            response = yield Ask(recipient=self.market, message=MsgPlaceOrder(
                symbol=self.symbol_name,
                side=order.side,
                quoteOrderQty=order.quote_value
            ))
            assert type(response) == MsgPlaceOrderResponse
            yield from self.run_strategy(Strategy.OrderStatus(
                side=response.side,
                type=response.type,
                status=Client.ORDER_STATUS_NEW,
                clientOrderId=response.clientOrderId,
                cumulativeFilledQty=Decimal(0),
                cumulativeQuoteAssetTransactedQty=Decimal(0),
                date=response.transactTime
            ))
        if type(self.action) == Strategy.Log:
            log: Strategy.Log = self.action
            self.log(log.message)
            yield from self.run_strategy(None)
        if type(self.action) == Strategy.Status:
            status: Strategy.Status = self.action
            self.send_status(status.status)
            yield from self.run_strategy(None)
        assert type(self.action) == Strategy.AskKLine, "Action is %s" % self.action

    def receiveMsg_MsgKLine(self, msg: MsgKLine, _sender):
        if type(self.action) == Strategy.AskKLine:
            self.last_kline = msg.kline
            yield from self.run_strategy(Strategy.KLine(kline=msg.kline))

    def receiveMsg_MsgExecutionReport(self, msg: MsgExecutionReport, _sender):
        yield from self.run_strategy(Strategy.OrderStatus(
            side=msg.side,
            type=msg.type,
            status=msg.status,
            clientOrderId=msg.clientOrderId,
            cumulativeFilledQty=msg.cumulativeFilledQty,
            cumulativeQuoteAssetTransactedQty=msg.cumulativeQuoteAssetTransactedQty,
            date=msg.transactionTime
        ))
