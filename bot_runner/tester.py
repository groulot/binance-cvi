import copy
import logging
import timeit
import uuid
from decimal import Decimal
from typing import Mapping, Type, Optional

import geneticalgorithm2
import numpy
from binance import Client
from datetimeutc import Datetime
from pandas import DataFrame

from b.actor import Ask
from b.applog import AppLog
from b.appstatus import AppStatus
from b.kline import Klines
from b.message import Response, DialogMessage
from b.spot.account import Balance
from b.spot.symbol import MsgGetKLines
from b.spot.symbol_filter import SymbolFilter
from bot_runner.base import StrategyRunnerBase
from bot_runner.strategy.strategy import Strategy


class StrategyTester(AppStatus, AppLog, StrategyRunnerBase):
    class Optimize:
        pass

    class RunOnHistory(DialogMessage):
        def __init__(self, *, start_datetime: Optional[Datetime] = None, end_datetime: Optional[Datetime] = None):
            super().__init__()
            assert start_datetime is None or type(start_datetime) == Datetime
            self.start_datetime = start_datetime
            assert end_datetime is None or type(end_datetime) == Datetime
            self.end_datetime = end_datetime

    def do_init(self):
        yield from super().do_init()

    def receiveMsg_Optimize(self, _message: Optimize, _sender):
        yield from self.optimize_strategy()
        logging.info("Optimization completed.")

    def optimize_strategy(self):
        response = yield Ask(recipient=self.symbol, message=MsgGetKLines())
        assert type(response) == Response and type(response.kline) == Klines
        klines = response.kline
        var_bounds = numpy.array([[0.0, 0.1], [0.0, 100.0]])

        def f(x):
            return -float(self.run_strategy(klines, x[0], x[1]))

        model = geneticalgorithm2.geneticalgorithm2(
            f,
            dimension=2,
            variable_type='real',
            variable_boundaries=var_bounds,
            variable_type_mixed=None,
            function_timeout=10,
            algorithm_parameters={'max_num_iteration': None,
                                  'population_size': 100,
                                  'mutation_probability': 0.1,
                                  'elit_ratio': 0.01,
                                  'crossover_probability': 0.5,
                                  'parents_portion': 0.3,
                                  'crossover_type': 'uniform',
                                  'mutation_type': 'uniform_by_center',
                                  'selection_type': 'roulette',
                                  'max_iteration_without_improv': None}
        )

        model.run(
            no_plot=False,
            disable_progress_bar=False,
            set_function=None,
            apply_function_to_parents=False,
            start_generation={'variables': None, 'scores': None},
            studEA=False,
            mutation_indexes=None,
            init_creator=None,
            init_oppositors=None,
            duplicates_oppositor=None,
            remove_duplicates_generation_step=None,
            revolution_oppositor=None,
            revolution_after_stagnation_step=None,
            revolution_part=0.3,
            population_initializer=geneticalgorithm2.Population_initializer(
                select_best_of=1,
                local_optimization_step='never',
                local_optimizer=None),
            stop_when_reached=None,
            callbacks=[],
            middle_callbacks=[],
            time_limit_secs=None,
            save_last_generation_as=None,
            seed=None
        )

    def receiveMsg_RunOnHistory(self, message: RunOnHistory, _sender):
        yield from self.run_on_history(
            start_datetime=message.start_datetime,
            end_datetime=message.end_datetime
        )
        logging.info("Run completed.")

    def run_on_history(self, *, start_datetime: Optional[Datetime] = None, end_datetime: Optional[Datetime] = None):
        response = yield Ask(recipient=self.symbol, message=MsgGetKLines(
            start_datetime=start_datetime,
            end_datetime=end_datetime
        ))
        assert type(response) == Response
        self.run_strategy(response.klines, display=True)

    def run_strategy(self, klines, a=None, b=None, display=False) -> Decimal:
        strategy = copy.deepcopy(self.strategy)
        strategy.display = display
        if a is not None:
            strategy.rebalance_percent_threshold = Decimal(a)
        if b is not None:
            strategy.rebalance_min_quote_amount = Decimal(b)
        current_kline = 0
        start_time = timeit.default_timer()
        coro = strategy.run()
        reply_to_coro = None
        last_percent=-1
        while True and current_kline < len(klines):
            percent = 100 * current_kline // len(klines)
            if percent > last_percent:
                print("Kline {}  {}%".format(current_kline, 100 * current_kline // len(klines)))
                last_percent = percent
            # Process and get desired action, give reply to previous action too
            action = coro.send(reply_to_coro)
            reply_to_coro = None
            if type(action) == Strategy.AskKLine:
                reply_to_coro = Strategy.KLine(kline=klines.iloc[current_kline])
                current_kline += 1
            if type(action) == Strategy.AskFund:
                reply_to_coro = self.fund
            if type(action) == Strategy.AskFee:
                reply_to_coro = self.fee
            if type(action) == Strategy.AskBalance:
                reply_to_coro = Balance(asset="BTC", free=Decimal(0), locked=Decimal(0))
            if type(action) == Strategy.AskOrderHistory:
                reply_to_coro = DataFrame.from_dict({
                    'status': [],
                    'side': [],
                    'executedQty': [],
                    'cummulativeQuoteQty': [],
                    'time': []
                })
            if type(action) == Strategy.AskKlineHistory:
                reply_to_coro = DataFrame.from_dict({
                    'open_time': [],
                    'close': []
                })
            if type(action) == Strategy.AskSymbolFilter:
                reply_to_coro = SymbolFilter()
                reply_to_coro.min_qty = Decimal('0.01')
                reply_to_coro.max_qty = Decimal('99999')
                reply_to_coro.min_price = Decimal(0)
                reply_to_coro.max_price = Decimal(999999)
                reply_to_coro.market_min_qty = Decimal('0.01')
                reply_to_coro.market_step_size = Decimal('0.01')
                reply_to_coro.min_notional = Decimal(10)
            if type(action) == Strategy.PlaceOrder:
                order: Strategy.PlaceOrder = action
                kline_nb = min(current_kline - 1, 0)
                reply_to_coro = Strategy.OrderStatus(
                    side=order.side,
                    type=Client.ORDER_TYPE_MARKET,
                    status=Client.ORDER_STATUS_FILLED,
                    clientOrderId=uuid.uuid4().hex,
                    cumulativeFilledQty=order.quote_value / Decimal(klines.iloc[kline_nb].close),
                    cumulativeQuoteAssetTransactedQty=order.quote_value,
                    date=Datetime.fromtimestamp(klines.iloc[kline_nb].open_time / 1000)
                )
            if type(action) == Strategy.Log:
                log: Strategy.Log = action
                self.log(log.message)
            if type(action) == Strategy.Status:
                status: Strategy.Status = action
                self.send_status(status.status)

        duration = timeit.default_timer() - start_time
        perfs: Mapping[Type[Strategy.PerformanceType], Strategy.PerformanceType] = strategy.get_performance()
        if display:
            logging.info("Processing {} candles done in {}".format(len(klines), duration))
            logging.info('Performance:')
            for p in perfs.values():
                logging.info(p)
        return perfs[Strategy.PerformanceTypeAfterSwing].perf
