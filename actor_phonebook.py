from __future__ import annotations

from b.actor import DialoguingActor
from b.message import DialogMessage, Response, Acknowledge, Error


class Lookup(DialogMessage):
    def __init__(self, *, identifier: str):
        super().__init__()
        self.identifier = identifier


class Register(DialogMessage):
    def __init__(self, *, identifier: str, address):
        super().__init__()
        self.identifier = identifier
        self.address = address


class ActorPhoneBook(DialoguingActor):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.phoneBook = {}

    def receiveMsg_Register(self, message: Register, sender):
        self.phoneBook[message.identifier] = message.address
        self.reply_to(message, sender, Acknowledge())

    def receiveMsg_Lookup(self, message: Lookup, sender):
        if message.identifier in self.phoneBook:
            self.reply_to(message, sender, Response({'address': self.phoneBook[message.identifier]}))
        else:
            self.reply_to(message, sender, Error(error='Not found in phonebook: %s' % message.identifier))
