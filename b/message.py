from __future__ import annotations

import uuid
from typing import Mapping, Type, Tuple

python_type = type


class DialogMessage:
    def __init__(self):
        self.ask_id = uuid.uuid4().int


class QuestionBase(DialogMessage):
    answers = ()


class Response(DialogMessage):
    def __init__(self, dictionnary):
        self.__dict__ = dictionnary
        super().__init__()

    def __str__(self):
        return str(self.__dict__)


def question(*, answers: Tuple[Type[DialogMessage], ...]):
    def decorator(klass):
        assert issubclass(klass, QuestionBase)
        klass.answers = answers
        return klass

    return decorator


class Acknowledge(DialogMessage):
    pass


class Error(Response):
    def __init__(self, error: str):
        super().__init__({'error': error})


@question(answers=(Acknowledge,))
class MsgInit(QuestionBase):
    pass


# Market

class MsgOrderReport:
    def __init__(self, *, client_order_id: str, report: Mapping[str, object]):
        self.client_order_id = client_order_id
        self.report = report
