from __future__ import annotations

import gc
import linecache
import tracemalloc
from decimal import Decimal
from typing import Optional

from datetimeutc import Datetime
from pandas import DataFrame


class Position:
    def __init__(self, *, price: Decimal, quantity: Decimal, date: Datetime):
        assert type(price) == Decimal
        self.price = price
        assert type(quantity) == Decimal
        self.quantity = quantity
        assert isinstance(date, Datetime)
        self.date = date

    def __str__(self):
        return "Position: price=%s quantity=%s datetime=%s" % (self.price, self.quantity, self.date)

def set_sign_according_to_side(orders: DataFrame) -> DataFrame:
    orders = orders.copy()
    orders.cummulativeQuoteQty = orders.apply(
        lambda r: abs(r.cummulativeQuoteQty) if r.side == 'BUY' else -abs(r.cummulativeQuoteQty),
        axis=1)
    orders.executedQty = orders.apply(
        lambda r: abs(r.executedQty) if r.side == 'BUY' else -abs(r.executedQty),
        axis=1)
    return orders

def drop_unfilled(orders: DataFrame) -> DataFrame:
    orders = orders.copy()
    return orders.query("status == 'FILLED'")

def position_from_order_list(orders: DataFrame) -> Position:
    assert type(orders) == DataFrame
    orders = orders.copy()
    orders = drop_unfilled(orders)
    if len(orders) == 0:
        return Position(
            price=Decimal(0),
            quantity=Decimal(0),
            date=Datetime(year=2001, month=1, day=1))

    orders = set_sign_according_to_side(orders)

    sum_quote = orders.cummulativeQuoteQty.sum()
    sum_qty = orders.executedQty.sum()

    if sum_qty == Decimal(0):
        return Position(
            price=Decimal(0),
            quantity=Decimal(0),
            date=Datetime(year=2001, month=1, day=1))
    be_price = sum_quote / sum_qty
    #print("Breakeven at %s current qty: %s" % (round(be_price, 2), round(sum_qty, 8)))
    position = Position(price=be_price, quantity=sum_qty, date=Datetime.fromtimestamp(orders.iloc[-1].time / 1000))
    del orders
    gc.collect()
    return position


def drop_obsolete_orders(orders: DataFrame) -> DataFrame:
    """
    Drop all orders dating back from before a return to a position of zero asset.
    :param orders: Iterable of orders sorted by date.
    :return: List of orders from the most recent zero position.
    """
    tmp = orders.copy()
    tmp = set_sign_according_to_side(tmp)
    tmp = drop_unfilled(tmp)
    tmp['assetQtySum'] = tmp.executedQty.cumsum()
    last_zero = tmp.where(tmp.assetQtySum == Decimal(0)).dropna()
    del tmp
    gc.collect()

    if len(last_zero) == 0:
        return orders
    last_zero = last_zero.iloc[-1].name
    return orders.loc[orders.index > last_zero]

def get_first_position(*, orders: DataFrame, worth_target: Decimal) -> Optional[Position]:
    """
    :param orders: list of orders
    :param quoteValue: Position value
    :return: the initial position, computed from the first orders that reached the quote worth target
    """
    orders = orders.copy()
    orders = drop_unfilled(orders)
    orders = drop_obsolete_orders(orders)
    orders = set_sign_according_to_side(orders)

    for index, row in orders.iterrows():
        position = position_from_order_list(orders.loc[:index])
        price = row.cummulativeQuoteQty / row.executedQty
        worth = price * position.quantity
        if worth >= worth_target * Decimal('0.995') and worth <= worth_target * Decimal('1.005'):
            # We found the initial orders
            del orders
            gc.collect()
            return position

    del orders
    gc.collect()
    return None

def display_top(snapshot, key_type='lineno', limit=10):
    snapshot = snapshot.filter_traces((
        tracemalloc.Filter(False, "<frozen importlib._bootstrap>"),
        tracemalloc.Filter(False, "<unknown>"),
    ))
    top_stats = snapshot.statistics(key_type)

    print("Top %s lines" % limit)
    for index, stat in enumerate(top_stats[:limit], 1):
        frame = stat.traceback[0]
        print("#%s: %s:%s: %.1f KiB"
              % (index, frame.filename, frame.lineno, stat.size / 1024))
        line = linecache.getline(frame.filename, frame.lineno).strip()
        if line:
            print('    %s' % line)

    other = top_stats[limit:]
    if other:
        size = sum(stat.size for stat in other)
        print("%s other: %.1f KiB" % (len(other), size / 1024))
    total = sum(stat.size for stat in top_stats)
    print("Total allocated size: %.1f KiB" % (total / 1024))

