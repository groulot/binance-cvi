import collections
from decimal import Decimal
from typing import Optional, Mapping


class SymbolFilter:
    def __init__(self):
        self.min_price: Optional[Decimal] = None
        self.max_price: Optional[Decimal] = None
        self.tick_size: Optional[Decimal] = None
        self.min_qty: Optional[Decimal] = None
        self.max_qty: Optional[Decimal] = None
        self.step_size: Optional[Decimal] = None
        self.min_notional: Optional[Decimal] = None
        self.market_min_qty: Optional[Decimal] = None
        self.market_max_qty: Optional[Decimal] = None
        self.market_step_size: Optional[Decimal] = None
        self.quote_asset_precision: int = 8

    @staticmethod
    def from_symbol_info(symbol_info: Mapping) -> 'SymbolFilter':
        assert isinstance(symbol_info, Mapping)
        result: SymbolFilter = SymbolFilter()
        result.set_quote_asset_precision(precision=int(symbol_info['quoteAssetPrecision']))
        for f in symbol_info['filters']:
            if f['filterType'] == 'PRICE_FILTER':
                result.set_price_filter(
                    min_price=Decimal(f['minPrice']),
                    max_price=Decimal(f['maxPrice']),
                    tick_size=Decimal(f['tickSize'])
                )
            if f['filterType'] == 'LOT_SIZE':
                result.set_lot_size_filter(
                    min_qty=Decimal(f['minQty']),
                    max_qty=Decimal(f['maxQty']),
                    step_size=Decimal(f['stepSize'])
                )
            if f['filterType'] == 'MARKET_LOT_SIZE':
                result.set_market_lot_size_filter(
                    min_qty=Decimal(f['minQty']),
                    max_qty=Decimal(f['maxQty']),
                    step_size=Decimal(f['stepSize'])
                )
            if f['filterType'] == 'MIN_NOTIONAL':
                result.set_min_notional(min_notional=Decimal(f['minNotional']))
        return result

    def set_price_filter(self, *, min_price: Decimal, max_price: Decimal, tick_size: Decimal):
        assert type(min_price) == Decimal
        if min_price != Decimal(0):
            self.min_price = min_price
        assert type(max_price) == Decimal
        if max_price != Decimal(0):
            self.max_price = max_price
        assert type(tick_size) == Decimal
        if tick_size != Decimal(0):
            self.tick_size = tick_size

    def set_lot_size_filter(self, *, min_qty: Decimal, max_qty: Decimal, step_size: Decimal):
        assert type(min_qty) == Decimal
        if min_qty != Decimal(0):
            self.min_qty = min_qty
        assert type(max_qty) == Decimal
        if max_qty != Decimal(0):
            self.max_qty = max_qty
        assert type(step_size) == Decimal
        if step_size != Decimal(0):
            self.step_size = step_size

    def set_market_lot_size_filter(self, *, min_qty: Decimal, max_qty: Decimal, step_size: Decimal):
        assert type(min_qty) == Decimal
        if min_qty != Decimal(0):
            self.market_min_qty = min_qty
        assert type(max_qty) == Decimal
        if max_qty != Decimal(0):
            self.market_max_qty = max_qty
        assert type(step_size) == Decimal
        if step_size != Decimal(0):
            self.market_step_size = step_size

    def set_min_notional(self, *, min_notional: Decimal):
        assert type(min_notional) == Decimal
        self.min_notional = min_notional

    def set_quote_asset_precision(self, *, precision: int):
        assert type(precision) == int
        self.quote_asset_precision = precision

    def filter_price(self, *, price: Decimal) -> Decimal:
        assert type(price) == Decimal
        if self.min_price is not None and price < self.min_price:
            raise Exception("Price %s under min price %s" % (price, self.min_price))
        if self.max_price is not None and price > self.max_price:
            raise Exception("Price %s over max price %s" % (price, self.max_price))
        if self.tick_size is not None:
            min_price = self.min_price if self.min_price is not None else Decimal(0)
            remainder: Decimal = (price - min_price) % self.tick_size
            return price - remainder
        return price

    def filter_lot(self, *, quantity: Decimal) -> Decimal:
        assert type(quantity) == Decimal
        if self.min_qty is not None and quantity < self.min_qty:
            raise Exception("Quantity %s under min quantity %s" % (quantity, self.min_qty))
        if self.max_qty is not None and quantity > self.max_qty:
            raise Exception("Quantity %s over max quantity %s" % (quantity, self.max_qty))
        if self.step_size is not None:
            min_qty = self.min_qty if self.min_qty is not None else Decimal(0)
            remainder: Decimal = (quantity - min_qty) % self.step_size
            return quantity - remainder
        return quantity

    def filter_market_lot(self, *, quantity: Decimal) -> Decimal:
        assert type(quantity) == Decimal
        if self.market_min_qty is not None and quantity < self.market_min_qty:
            raise Exception("Market quantity %s under min quantity %s" % (quantity, self.market_min_qty))
        if self.market_max_qty is not None and quantity > self.market_max_qty:
            raise Exception("Market quantity %s over max quantity %s" % (quantity, self.market_max_qty))
        if self.market_step_size is not None:
            min_qty = self.market_min_qty if self.market_min_qty is not None else Decimal(0)
            remainder: Decimal = (quantity - min_qty) % self.market_step_size
            return self.filter_lot(quantity=quantity - remainder)
        return self.filter_lot(quantity=quantity)

    def filter_quote_order_quantity(self, *, quantity: Decimal) -> Decimal:
        assert type(quantity) == Decimal
        return Decimal(round(quantity, self.quote_asset_precision))
