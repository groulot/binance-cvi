from __future__ import annotations

import gc
from abc import ABC, abstractmethod
from decimal import Decimal
from typing import Optional

from binance import Client
from datetimeutc import Datetime

from b.actor import DialoguingActor
from b.message import DialogMessage, python_type, question, QuestionBase, MsgInit, Acknowledge


class MsgPlaceOrderResponse(DialogMessage):
    def __init__(
            self,
            *,
            symbol: str,
            clientOrderId: str,
            transactTime: Datetime,
            price: Decimal,
            origQty: Decimal,
            executedQty: Decimal,
            status: str,
            type: str,
            side: str):
        super().__init__()
        self.symbol = symbol
        assert python_type(clientOrderId) == str and clientOrderId != ""
        self.clientOrderId = clientOrderId
        assert isinstance(transactTime, Datetime)
        self.transactTime = transactTime
        assert python_type(price) == Decimal
        self.price = price
        assert origQty is None or (python_type(origQty) == Decimal and origQty > Decimal(0))
        self.origQty = origQty
        assert python_type(executedQty) == Decimal
        self.executedQty = executedQty
        assert status in [Client.ORDER_STATUS_CANCELED,
                          Client.ORDER_STATUS_EXPIRED,
                          Client.ORDER_STATUS_FILLED,
                          Client.ORDER_STATUS_NEW,
                          Client.ORDER_STATUS_PARTIALLY_FILLED,
                          Client.ORDER_STATUS_PENDING_CANCEL,
                          Client.ORDER_STATUS_REJECTED]
        self.status = status
        assert type in [Client.ORDER_TYPE_LIMIT,
                        Client.ORDER_TYPE_MARKET,
                        Client.ORDER_TYPE_STOP_LOSS,
                        Client.ORDER_TYPE_STOP_LOSS_LIMIT,
                        Client.ORDER_TYPE_TAKE_PROFIT,
                        Client.ORDER_TYPE_TAKE_PROFIT_LIMIT,
                        Client.ORDER_TYPE_LIMIT_MAKER]
        self.type = type
        assert side in [Client.SIDE_SELL, Client.SIDE_BUY]
        self.side = side


@question(answers=(MsgPlaceOrderResponse,))
class MsgPlaceOrder(QuestionBase):
    def __init__(
            self,
            *,
            symbol: str,
            side: str,
            type: str = Client.ORDER_TYPE_MARKET,
            timeInForce: Optional[str] = None,
            quantity: Optional[Decimal] = None,
            quoteOrderQty: Optional[Decimal] = None,
            price: Optional[Decimal] = None,
            recvWindow: int = None):
        super().__init__()
        self.symbol = symbol
        assert side in [Client.SIDE_SELL, Client.SIDE_BUY]
        self.side = side
        assert type in [Client.ORDER_TYPE_LIMIT,
                        Client.ORDER_TYPE_MARKET,
                        Client.ORDER_TYPE_STOP_LOSS,
                        Client.ORDER_TYPE_STOP_LOSS_LIMIT,
                        Client.ORDER_TYPE_TAKE_PROFIT,
                        Client.ORDER_TYPE_TAKE_PROFIT_LIMIT,
                        Client.ORDER_TYPE_LIMIT_MAKER]
        self.type = type
        assert timeInForce is None or timeInForce in [Client.TIME_IN_FORCE_GTC, Client.TIME_IN_FORCE_FOK,
                                                      Client.TIME_IN_FORCE_IOC]
        self.timeInForce = timeInForce
        assert ((quoteOrderQty is not None and quantity is None)
                or (quoteOrderQty is None and python_type(quantity) == Decimal and quantity > Decimal(0)))
        self.quantity = quantity
        assert quoteOrderQty is None or (python_type(quoteOrderQty) == Decimal and quoteOrderQty > Decimal(0))
        assert type != Client.ORDER_TYPE_MARKET or quoteOrderQty is not None
        self.quoteOrderQty = quoteOrderQty
        assert type != Client.ORDER_TYPE_LIMIT or (python_type(price) == Decimal and price > Decimal(0))
        self.price = price
        assert recvWindow is None or (python_type(recvWindow) == int and recvWindow > 0)
        self.recvWindow = recvWindow


class MsgCancelOrderResponse(DialogMessage):
    def __init__(
            self,
            *,
            symbol: str,
            origClientOrderId: str,
            clientOrderId: str,
            price: Decimal,
            origQty: Decimal,
            executedQty: Decimal,
            cummulativeQuoteQty: Decimal,
            status: str,
            timeInForce: str,
            type: str,
            side: str):
        super().__init__()
        self.symbol = symbol
        assert python_type(origClientOrderId) == str and origClientOrderId != ""
        self.origClientOrderId = origClientOrderId
        assert python_type(clientOrderId) == str and clientOrderId != ""
        self.clientOrderId = clientOrderId
        assert price is None or (python_type(price) == Decimal and price > Decimal(0))
        self.price = price
        assert origQty is None or (python_type(origQty) == Decimal and origQty > Decimal(0))
        self.origQty = origQty
        assert python_type(executedQty) == Decimal
        self.executedQty = executedQty
        assert python_type(cummulativeQuoteQty) == Decimal
        self.cummulativeQuoteQty = cummulativeQuoteQty
        assert status in [Client.ORDER_STATUS_CANCELED,
                          Client.ORDER_STATUS_EXPIRED,
                          Client.ORDER_STATUS_FILLED,
                          Client.ORDER_STATUS_NEW,
                          Client.ORDER_STATUS_PARTIALLY_FILLED,
                          Client.ORDER_STATUS_PENDING_CANCEL,
                          Client.ORDER_STATUS_REJECTED]
        self.status = status
        assert timeInForce in [Client.TIME_IN_FORCE_GTC, Client.TIME_IN_FORCE_FOK, Client.TIME_IN_FORCE_IOC]
        self.timeInForce = timeInForce
        assert type in [Client.ORDER_TYPE_LIMIT,
                        Client.ORDER_TYPE_MARKET,
                        Client.ORDER_TYPE_STOP_LOSS,
                        Client.ORDER_TYPE_STOP_LOSS_LIMIT,
                        Client.ORDER_TYPE_TAKE_PROFIT,
                        Client.ORDER_TYPE_TAKE_PROFIT_LIMIT,
                        Client.ORDER_TYPE_LIMIT_MAKER]
        self.type = type
        assert side in [Client.SIDE_SELL, Client.SIDE_BUY]
        self.side = side


@question(answers=(MsgCancelOrderResponse,))
class MsgCancelOrder(QuestionBase):
    def __init__(self, *, symbol: str, clientOrderId: str, recvWindow: int = None):
        super(MsgCancelOrder, self).__init__()
        self.symbol = symbol
        assert python_type(clientOrderId) == str and clientOrderId != ""
        self.clientOrderId = clientOrderId
        assert recvWindow is None or (python_type(recvWindow) == int and recvWindow > 0)
        self.recvWindow = recvWindow


class MsgExecutionReport:
    def __init__(self,
                 *,
                 symbol: str,
                 clientOrderId: str,
                 side: str,
                 type: str,
                 timeInForce: str,
                 quantity: Decimal,
                 price: Decimal,
                 stopPrice: Decimal,
                 origClientOrderId: str,
                 executionType: str,
                 status: str,
                 rejectReason: str,
                 lastExecutedQty: Decimal,
                 cumulativeFilledQty: Decimal,
                 lastExecutedPrice: Decimal,
                 commission: Decimal,
                 commissionAsset: Optional[str],
                 transactionTime: Datetime,
                 cumulativeQuoteAssetTransactedQty: Decimal,
                 lastQuoteAssetTransactedQty: Decimal,
                 quoteOrderQuantity: Decimal):
        assert symbol is not None and python_type(symbol) == str and symbol.isupper()
        self.symbol = symbol
        assert python_type(clientOrderId) == str
        self.clientOrderId = clientOrderId
        assert side in [Client.SIDE_SELL, Client.SIDE_BUY]
        self.side = side
        assert type in [Client.ORDER_TYPE_LIMIT,
                        Client.ORDER_TYPE_MARKET,
                        Client.ORDER_TYPE_STOP_LOSS,
                        Client.ORDER_TYPE_STOP_LOSS_LIMIT,
                        Client.ORDER_TYPE_TAKE_PROFIT,
                        Client.ORDER_TYPE_TAKE_PROFIT_LIMIT,
                        Client.ORDER_TYPE_LIMIT_MAKER]
        self.type = type
        assert timeInForce in [Client.TIME_IN_FORCE_GTC, Client.TIME_IN_FORCE_FOK, Client.TIME_IN_FORCE_IOC]
        self.timeInForce = timeInForce
        assert python_type(quantity) == Decimal
        self.quantity = quantity
        assert python_type(price) == Decimal
        self.price = price
        assert python_type(stopPrice) == Decimal
        self.stopPrice = stopPrice
        assert python_type(origClientOrderId) == str
        self.origClientOrderId = origClientOrderId
        assert executionType in [
            'NEW',
            'CANCELED',
            'REJECTED',
            'TRADE',
            'EXPIRED']
        self.executionType = executionType
        assert status in [
            Client.ORDER_STATUS_NEW,
            Client.ORDER_STATUS_PARTIALLY_FILLED,
            Client.ORDER_STATUS_FILLED,
            Client.ORDER_STATUS_CANCELED,
            Client.ORDER_STATUS_PENDING_CANCEL,
            Client.ORDER_STATUS_REJECTED,
            Client.ORDER_STATUS_EXPIRED]
        self.status = status
        assert rejectReason == "NONE" or python_type(rejectReason) == dict
        self.rejectReason = rejectReason
        assert python_type(lastExecutedQty) == Decimal
        self.lastExecutedQty = lastExecutedQty
        assert python_type(cumulativeFilledQty) == Decimal
        self.cumulativeFilledQty = cumulativeFilledQty
        assert python_type(lastExecutedPrice) == Decimal
        self.lastExecutedPrice = lastExecutedPrice
        assert python_type(commission) == Decimal
        self.commission = commission
        assert commissionAsset is None or python_type(commissionAsset) == str
        self.commissionAsset = commissionAsset
        assert isinstance(transactionTime, Datetime)
        self.transactionTime = transactionTime
        assert python_type(cumulativeQuoteAssetTransactedQty) == Decimal
        self.cumulativeQuoteAssetTransactedQty = cumulativeQuoteAssetTransactedQty
        assert python_type(lastQuoteAssetTransactedQty) == Decimal
        self.lastQuoteAssetTransactedQty = lastQuoteAssetTransactedQty
        assert python_type(quoteOrderQuantity) == Decimal
        self.quoteOrderQuantity = quoteOrderQuantity


class Market(DialoguingActor, ABC):

    def receiveMsg_MsgInit(self, _message: MsgInit, sender):
        yield from self.do_init()
        gc.collect()
        self.reply(sender, Acknowledge())

    @abstractmethod
    def do_init(self):
        pass

    @abstractmethod
    def receiveMsg_MsgPlaceOrder(self, message: MsgPlaceOrder, sender):
        pass

    @abstractmethod
    def receiveMsg_MsgCancelOrder(self, message: MsgCancelOrder, sender):
        pass
