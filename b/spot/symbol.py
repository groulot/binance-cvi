from __future__ import annotations

import gc
import tracemalloc
from concurrent.futures import ProcessPoolExecutor
from datetime import timedelta
from typing import Dict, Optional, Union, Iterable

import numpy
from binance.client import Client
from datetimeutc import Datetime
from pandas import DataFrame
from thespian.actors import ActorAddress, ActorExitRequest
from thespian.initmsgs import initializing_messages

from b.actor import DialoguingActor
from b.exchange import Exchange
from b.kline import Klines
from b.message import Response, DialogMessage, Acknowledge, MsgInit, \
    question, QuestionBase, Error
from b.spot.symbol_filter import SymbolFilter
from b.stream import Stream, Config, StreamMessage
from b.supervisor import Supervisor


@question(answers=(Response,))
class MsgGetSymbol(QuestionBase):
    pass


@question(answers=(Response,))
class MsgGetBaseAsset(QuestionBase):
    pass


@question(answers=(Response,))
class MsgGetQuoteAsset(QuestionBase):
    pass


class MsgGetSymbolFilterResponse(DialogMessage):
    def __init__(self, *, symbol_filter: SymbolFilter):
        super().__init__()
        assert type(symbol_filter) == SymbolFilter
        self.symbol_filter = symbol_filter


@question(answers=(MsgGetSymbolFilterResponse,))
class MsgGetSymbolFilter(QuestionBase):
    def __init__(self, *, symbol: str):
        super().__init__()
        self.symbol = symbol


@question(answers=(Acknowledge, Error))
class MsgSubscribeKLines(QuestionBase):
    def __init__(self, *, columns: Union[Iterable[str], slice] = None):
        super().__init__()
        assert columns is None or type(columns) == slice or isinstance(columns, Iterable)
        self.columns = columns


@question(answers=(Acknowledge,))
class MsgUnsubscribeKLines(QuestionBase):
    pass


@question(answers=(Response, Error))
class MsgGetKLines(QuestionBase):
    def __init__(self,
                 *,
                 start_datetime: Datetime,
                 end_datetime: Optional[Datetime] = None,
                 columns: Optional[Iterable[str]] = None):
        super().__init__()
        assert isinstance(start_datetime, Datetime)
        self.start_datetime = start_datetime
        assert end_datetime is None or isinstance(end_datetime, Datetime)
        self.end_datetime = end_datetime
        assert columns is None or type(columns) in [str, slice] or isinstance(columns, Iterable)
        self.columns = columns


class MsgKLine:
    def __init__(self, kline):
        self.kline = kline


@initializing_messages([('client', Client),
                        ('name', str),
                        ('history', timedelta),
                        ('exchange', Exchange)],
                       initdone='init_completed')
class Symbol(Supervisor, DialoguingActor):
    MAX_LIST_LENGTH = 5000

    @staticmethod
    def _dt_to_whole_minute_micro_timestamp(dt: Datetime) -> int:
        assert isinstance(dt, Datetime)
        return int(dt.timestamp() * 60000) // 60

    def __init__(self, *args, **kwargs):
        self.subscriptions: Dict[str, (ActorAddress, Union[Iterable[str], slice])] = {}
        self.kline: Optional[Klines] = None
        self.symbol_filter: Optional[SymbolFilter] = None
        self.base_asset: Optional[str] = ""
        self.quote_asset: Optional[str] = ""
        super().__init__(*args, **kwargs)

    def init_completed(self):
        # Add indicators
        # self.kline.data['SMA9'] = self.kline.data.close.rolling(window=9).mean()
        # self.kline.data['SMA20'] = self.kline.data.close.rolling(window=20).mean()

        info = self.client.get_symbol_info(self.name)
        self.symbol_filter = SymbolFilter.from_symbol_info(info)

        self.base_asset = info['baseAsset']
        self.quote_asset = info['quoteAsset']

        config = Config(
            subscriber=self.myAddress,
            exchange=self.exchange.exchange,
            channel='kline_1m',
            market='{}'.format(self.name.lower())
        )
        self.create_child(name='stream', actor_class=Stream, init_messages=[config])
        gc.collect()

    def child_recreated(self, child):
        pass

    def receiveMsg_MsgInit(self, _message: MsgInit, sender):
        self.reply(recipient=sender, message=Acknowledge())

    def receiveMsg_StreamMessage(self, message: StreamMessage, _sender):
        assert type(message.message) == dict
        msg = message.message
        kline_msg = msg['data']
        self._kline_processor(kline_msg)

    def _kline_processor(self, msg):
        if 'k' in msg:
            k = msg['k']
            kline_data = {
                'open_time': numpy.uint64(k['t']),
                'symbol': k['s'],
                'interval': k['i'],
                'open': numpy.float64(k['o']),
                'high': numpy.float64(k['h']),
                'low': numpy.float64(k['l']),
                'close': numpy.float64(k['c']),
                'volume': numpy.float64(k['v']),
                'close_time': numpy.uint64(k['T']),
                'num_trades': numpy.uint64(k['n']),
                'closed': bool(k['x'])
            }
            # print("New candle data: %s" % kline_data)
            self.kline = Klines.from_dict(kline_data)

            # Update indicators
            # sma9 = self.kline.data.columns.get_loc('SMA9')
            # self.kline.data.iloc[-10:, sma9] = (
            #    self.kline.data.close
            #        .iloc[-19:]
            #        .rolling(window=9).mean()
            #        .iloc[-10:]
            #
            # )
            # sma20 = self.kline.data.columns.get_loc('SMA20')
            # self.kline.data.iloc[-21:, sma20] = (
            #    self.kline.data.close
            #        .iloc[-41:]
            #        .rolling(window=20).mean()
            #        .iloc[-21:]
            # )

            for (subscriber, col_filter) in self.subscriptions.values():
                filtered_data = self.kline.data.iloc[-1].loc[col_filter]
                self.send(subscriber, MsgKLine(filtered_data))
                del filtered_data
            gc.collect()

    def receiveMsg_MsgGetSymbol(self, _message: MsgGetSymbol, sender):
        self.reply(sender, Response({'symbol': self.name}))

    def receiveMsg_MsgGetBaseAsset(self, _message: MsgGetBaseAsset, sender):
        self.reply(sender, Response({'base_asset': self.base_asset}))

    def receiveMsg_MsgGetQuoteAsset(self, _message: MsgGetQuoteAsset, sender):
        self.reply(sender, Response({'quote_asset': self.quote_asset}))

    def receiveMsg_MsgGetSymbolFilter(self, message: MsgGetSymbolFilter, sender):
        assert message.symbol == self.name
        self.reply(sender, MsgGetSymbolFilterResponse(symbol_filter=self.symbol_filter))

    def receiveMsg_MsgSubscribeKLines(self, message: MsgSubscribeKLines, sender):
        print("New subscription from %s" % sender)
        if message.columns is None:
            message.columns = slice(None)

        self.subscriptions[str(sender)] = (sender, message.columns)
        self.reply(sender, Acknowledge())

    def receiveMsg_MsgUnsubscribeKLines(self, _message: MsgSubscribeKLines, sender):
        assert (str(sender) in self.subscriptions)
        print("Unsubscription from %s" % sender)
        del self.subscriptions[str(sender)]
        self.reply(sender, Acknowledge())

    @staticmethod
    def _load_klines(symbol, start_datetime, end_datetime, columns) -> DataFrame:
        klines = Klines(
            symbol=symbol,
            interval=Client.KLINE_INTERVAL_1MINUTE,
            start_date=start_datetime,
            end_date=end_datetime,
            columns=columns)
        # TODO klines.save()
        return klines.data

    def receiveMsg_MsgGetKLines(self, message: MsgGetKLines, sender):
        try:
            if message.end_datetime is None:
                message.end_datetime = Datetime.now()
            # Make python bloat with memory in another temp process
            with ProcessPoolExecutor(max_workers=1) as executor:
                result = executor.submit(
                    self._load_klines,
                    symbol=self.name,
                    start_datetime=message.start_datetime,
                    end_datetime=message.end_datetime,
                    columns=message.columns
                ).result()
            self.reply(sender, Response({'klines': result}))
            gc.collect()
        except Exception as ex:
            self.reply(recipient=sender, message=Error(str(ex)))


    #def receiveMsg_ActorExitRequest(self, message: ActorExitRequest, _sender):
    #    (mem, peak) = tracemalloc.get_traced_memory()
    #    print("Current mem use: %s Peak: %s" % (mem, peak))

    #    snapshot = tracemalloc.take_snapshot()
    #    top_stats = snapshot.statistics('filename')
    #    print("[ Top 10 ]")
    #    for stat in top_stats[:10]:
    #        print(stat)
    #    with open("/proc/self/smaps") as f:
    #        print(f.read())
