from __future__ import annotations

from datetimeutc import Datetime
from decimal import Decimal
from typing import Dict

from thespian.actors import ActorAddress
from thespian.initmsgs import initializing_messages
from binance import Client

from b.actor import Ask
from b.credentials import Credentials
from b.exchange import Exchange
from b.message import Response, Acknowledge, MsgInit
from b.spot.market import MsgPlaceOrderResponse, MsgPlaceOrder, MsgCancelOrderResponse, MsgCancelOrder, \
    MsgExecutionReport, Market
from b.spot.symbol import MsgGetSymbol
from b.stream import Stream, StreamMessage, Config
from b.supervisor import Supervisor

python_type = type


@initializing_messages([('client', Client),
                        ('credentials', Credentials),
                        ('exchange', Exchange),
                        ('symbol', ActorAddress)])
class Market(Supervisor, Market):
    def __init__(self, *args, **kwargs):
        super().__init__(args, kwargs)
        self.orderid_to_actor: Dict[str, ActorAddress] = {}
        self.symbol_name: str = ""

    def receiveMsg_MsgInit(self, _message: MsgInit, sender):
        yield from self.do_init()
        self.reply(sender, Acknowledge())

    def do_init(self):
        response = yield Ask(recipient=self.symbol, message=MsgGetSymbol())
        assert type(response) == Response
        assert type(response.symbol) == str
        self.symbol_name = response.symbol
        config = Config(
            subscriber=self.myAddress,
            exchange=self.exchange.exchange,
            channel='!userData',
            market='arr',
            credentials=self.credentials
        )
        self.create_child(name='stream', actor_class=Stream, init_messages=[config])

    def child_recreated(self, child):
        pass

    def receiveMsg_StreamMessage(self, message: StreamMessage, _sender):
        assert type(message.message) == dict
        msg = message.message
        if msg['e'] == 'executionReport' and msg['s'] == self.symbol_name:
            #print(msg)
            client_order_id: str = msg['c']
            if client_order_id in self.orderid_to_actor:
                self.send(
                    recipient=self.orderid_to_actor[client_order_id],
                    message=MsgExecutionReport(
                        symbol=msg['s'],
                        clientOrderId=client_order_id,
                        side=msg['S'],
                        type=msg['o'],
                        timeInForce=msg['f'],
                        quantity=Decimal(msg['q']),
                        price=Decimal(msg['p']),
                        stopPrice=Decimal(msg['P']),
                        origClientOrderId=msg['C'],
                        executionType=msg['x'],
                        status=msg['X'],
                        rejectReason=msg['r'],
                        lastExecutedQty=Decimal(msg['l']),
                        cumulativeFilledQty=Decimal(msg['z']),
                        lastExecutedPrice=Decimal(msg['L']),
                        commission=Decimal(msg['n']),
                        commissionAsset=msg['N'],
                        transactionTime=Datetime.fromtimestamp(int(msg['T']) / 1000),
                        cumulativeQuoteAssetTransactedQty=Decimal(msg['Z']),
                        lastQuoteAssetTransactedQty=Decimal(msg['Y']),
                        quoteOrderQuantity=Decimal(msg['Q'])
                    )
                )

    def receiveMsg_MsgPlaceOrder(self, message: MsgPlaceOrder, sender):
        assert message.symbol == self.symbol_name
        order_arguments = message.__dict__.copy()
        del order_arguments['ask_id']
        order_arguments = {k: v for (k, v) in order_arguments.items() if v is not None and not k.startswith('__')}
        # print(order_arguments)
        response = self.client.create_order(**order_arguments)
        # print(response)
        self.orderid_to_actor[response['clientOrderId']] = sender
        self.reply(sender, MsgPlaceOrderResponse(
            symbol=response['symbol'],
            clientOrderId=response['clientOrderId'],
            transactTime=Datetime.fromtimestamp(float(response['transactTime']) / 1000.0),
            price=Decimal(response['price']),
            origQty=Decimal(response['origQty']),
            executedQty=Decimal(response['executedQty']),
            status=response['status'],
            type=response['type'],
            side=response['side']
        ))

    def receiveMsg_MsgCancelOrder(self, message: MsgCancelOrder, sender):
        assert message.clientOrderId in self.orderid_to_actor
        response = self.client.cancel_order(
            symbol=message.symbol,
            origClientOrderId=message.clientOrderId
        )
        self.reply(sender, MsgCancelOrderResponse(
            symbol=response['symbol'],
            origClientOrderId=response['origClientOrderId'],
            clientOrderId=response['clientOrderId'],
            price=Decimal(response['price']),
            origQty=Decimal(response['origQty']),
            executedQty=Decimal(response['executedQty']),
            cummulativeQuoteQty=Decimal(response['cummulativeQuoteQty']),
            status=response['status'],
            timeInForce=response['timeInForce'],
            type=response['type'],
            side=response['side']
        )
                   )
        del self.orderid_to_actor[message.clientOrderId]
