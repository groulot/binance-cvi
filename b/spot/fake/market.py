from __future__ import annotations

import json
import uuid
from decimal import Decimal
from typing import Optional, Dict, Set

from datetimeutc import Datetime
from pandas import Series
from thespian.initmsgs import initializing_messages
from binance import Client

import actor_phonebook
from b.credentials import Credentials
from b.actor import DialoguingActor, Ask
from b.exchange import Exchange
from b.message import Response
from b.spot.market import MsgPlaceOrderResponse, MsgPlaceOrder, MsgCancelOrderResponse, MsgCancelOrder, \
    MsgExecutionReport
from b.spot.symbol import MsgKLine, MsgSubscribeKLines, MsgUnsubscribeKLines
from b.stream import Config, Stream, StreamMessage
from b.supervisor import Supervisor


@initializing_messages([('client', Client),
                        ('credentials', Credentials),
                        ('exchange', Exchange)],
                       initdone='init_completed')
class FakeMarket(Supervisor, DialoguingActor):
    def __init__(self, *args, **kwargs):
        super().__init__(args, kwargs)
        self.orderid_to_actor = {}
        self.orders: Dict[str, MsgPlaceOrder] = {}
        self.asset_orders: Dict[str, Set[MsgPlaceOrder]] = {}
        self.last_klines: Optional[Dict[str, Series]] = {}

    def init_completed(self):
        config = Config(
            subscriber=self.myAddress,
            exchange=self.exchange.exchange,
            channel='!userData',
            market='arr',
            credentials=self.credentials
        )
        self.create_child(name='stream', actor_class=Stream, init_messages=[config])

    def phonebook(self):
        return self.createActor(actor_phonebook.ActorPhoneBook, globalName='phonebook')

    @staticmethod
    def get_client_order_id() -> str:
        return str(uuid.uuid4())

    def receiveMsg_MsgPlaceOrder(self, message: MsgPlaceOrder, sender):
        print("Received place order: %s" % message)
        print("Asking for symbol {} in phonebook {}".format(message.symbol, self.phonebook()))
        response = yield Ask(recipient=self.phonebook(),
                             message=actor_phonebook.Lookup('symbol_{}'.format(message.symbol.lower())))
        assert type(response) == Response and response.address is not None
        print("Response from phonebook: {}".format(response))
        symbol = response.address
        self.send(symbol, MsgSubscribeKLines())

        self.orderid_to_actor[message.newClientOrder] = sender
        self.orders[message.newClientOrder] = message
        if message.symbol not in self.asset_orders:
            self.asset_orders[message.symbol] = set()
        self.asset_orders[message.symbol].add(message)
        self.reply(sender, MsgPlaceOrderResponse(
            symbol=message.symbol,
            clientOrderId=message.newClientOrder,
            transactTime=Datetime.now(),
            price=message.price,
            origQty=message.quantity,
            executedQty=Decimal(0),
            status='NEW',
            type=message.type,
            side=message.side
        ))

    def receiveMsg_MsgCancelOrder(self, message: MsgCancelOrder, sender):
        order = self.orders[message.clientOrderId]
        assert message.symbol == order.symbol
        self.reply(sender, MsgCancelOrderResponse(
            symbol=message.symbol,
            origClientOrderId=message.clientOrderId,
            clientOrderId=self.get_client_order_id(),
            price=order.price,
            origQty=order.quantity,
            executedQty=Decimal(0),
            cummulativeQuoteQty=Decimal(0),
            status='CANCELED',
            timeInForce=order.timeInForce,
            type=order.type,
            side=order.side)
                   )
        del self.orderid_to_actor[message.clientOrderId]
        del self.orders[message.clientOrderId]
        self.asset_orders[message.symbol].remove(order)
        if all(o[message.symbol] != message.symbol for o in self.orders.values()):
            response = yield Ask(
                recipient=self.phonebook(),
                message=actor_phonebook.Lookup('symbol_{}'.format(message.symbol.lower()))
            )
            assert type(response) == Response and response.address is not None
            symbol = response.address
            self.send(symbol, MsgUnsubscribeKLines())

    def receiveMsg_MsgKLine(self, message: MsgKLine, _sender):
        self.last_klines[message.kline.symbol] = message.kline
        if message.kline.symbol in self.asset_orders:
            for o in self.asset_orders[message.kline.symbol]:
                assert type(o) == MsgPlaceOrder
                recipient = self.orderid_to_actor[o.newClientOrder]
                quantity = o.quoteOrderQty / Decimal(message.kline.close)
                price = Decimal(message.kline.close)
                commission = Decimal('0.075') * o.quoteOrderQty
                self.send(recipient=recipient, message=MsgExecutionReport(
                    symbol=message.kline.symbol,
                    clientOrderId=o.newClientOrder,
                    side=o.side,
                    type=o.type,
                    timeInForce=o.timeInForce,
                    quantity=quantity,
                    price=price,
                    stopPrice=Decimal(0),
                    origClientOrderId="",
                    executionType='TRADE',
                    status=Client.ORDER_STATUS_FILLED,
                    rejectReason="",
                    lastExecutedQty=quantity,
                    cumulativeFilledQty=quantity,
                    lastExecutedPrice=price,
                    commission=commission,
                    commissionAsset="USDT",
                    cumulativeQuoteAssetTransactedQty=o.quoteOrderQty,
                    lastQuoteAssetTransactedQty=o.quoteOrderQty,
                    quoteOrderQuantity=o.quoteOrderQty
                ))
                del self.orderid_to_actor[o.newClientOrder]
                del self.orders[o.newClientOrder]
            self.asset_orders[message.kline.symbol].clear()

    def receiveMsg_StreamMessage(self, message: StreamMessage, _sender):
        assert type(message.message) == dict
        msg = message.message
        if msg['e'] == 'executionReport' and msg['c'] in self.orderid_to_actor:
            recipient = self.orderid_to_actor[msg['c']]
            rejectReason = msg['r']
            if rejectReason != "":
                rejectReason = json.loads(rejectReason)
            self.send(recipient=recipient, message=MsgExecutionReport(
                symbol=msg['s'],
                clientOrderId=msg['c'],
                side=msg['S'],
                type=msg['o'],
                timeInForce=msg['f'],
                quantity=Decimal(msg['q']),
                price=Decimal(msg['p']),
                stopPrice=Decimal(msg['P']),
                origClientOrderId=msg['C'],
                executionType=msg['x'],
                status=msg['X'],
                rejectReason=rejectReason,
                lastExecutedQty=Decimal(msg['l']),
                cumulativeFilledQty=Decimal(msg['z']),
                lastExecutedPrice=Decimal(msg['L']),
                commission=Decimal(msg['n']),
                commissionAsset=msg['N'],
                cumulativeQuoteAssetTransactedQty=Decimal(msg['Z']),
                lastQuoteAssetTransactedQty=Decimal(msg['Y']),
                quoteOrderQuantity=Decimal(msg['Q'])
            ))
