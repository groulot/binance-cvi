from __future__ import annotations

import gc
import time
from decimal import Decimal
from typing import Dict

from binance.client import Client
from datetimeutc import Datetime
from pandas import DataFrame
from thespian.actors import ActorAddress
from thespian.initmsgs import initializing_messages

from b.actor import DialoguingActor
from b.credentials import Credentials
from b.exchange import Exchange
from b.message import Response, question, QuestionBase, Acknowledge
from b.stream import Config, Stream, StreamMessage
from b.supervisor import Supervisor


class Balance:
    def __init__(self, asset: str, free: Decimal, locked: Decimal):
        self.asset = asset
        self.free = free
        self.locked = locked

    def __str__(self):
        return "%s: free %d locked %d" % (self.asset, float(self.free), float(self.locked))


class Balances(dict):
    def __str__(self) -> str:
        return ("Balances: "
                + ','.join(["%s " % b for b in self.values() if b.free != Decimal(0) or b.locked != Decimal(0)]))


@question(answers=(Response,))
class MsgGetOrderHistory(QuestionBase):
    def __init__(self, *, symbol: str, start_date: Datetime = Datetime(year=2001, month=1, day=1)):
        super().__init__()
        assert type(symbol) == str
        self.symbol = symbol
        assert isinstance(start_date, Datetime)
        self.start_date = start_date


@question(answers=(Response,))
class MsgGetBalance(QuestionBase):
    def __init__(self, *, asset_name: str):
        super().__init__()
        assert asset_name is not None and type(asset_name) == str and asset_name.isupper()
        self.asset_name = asset_name


@question(answers=(Acknowledge,))
class MsgSubscribeToAsset(QuestionBase):
    def __init__(self, *, asset_name):
        super().__init__()
        assert asset_name.isupper()
        self.name = asset_name


@initializing_messages([('client', Client),
                        ('credentials', Credentials),
                        ('exchange', Exchange)],
                       initdone='init_completed')
class Account(Supervisor, DialoguingActor):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.balances: Balances[str, Balance] = Balances()
        self.asset_subscribers: Dict[str, ActorAddress] = {}

    def init_completed(self):
        records = self.client.get_account()['balances']
        #print(records)
        for r in records:
            self.balances[r['asset']] = Balance(
                asset=r['asset'],
                free=Decimal(r['free']),
                locked=Decimal(r['locked'])
            )

        config = Config(
            subscriber=self.myAddress,
            exchange=self.exchange.exchange,
            channel='!userData',
            market='arr',
            credentials=self.credentials
        )
        self.create_child(name='stream', actor_class=Stream, init_messages=[config])
        gc.collect()

    def child_recreated(self, child):
        pass

    def _get_order_history(self, *, symbol: str, start_date: Datetime) -> DataFrame:
        assert type(symbol) == str
        assert isinstance(start_date, Datetime)
        start_timestamp = int(start_date.timestamp() * 1000)
        history = self.client.get_all_orders(symbol=symbol, startTime=start_timestamp)
        df = DataFrame.from_dict(history).set_index('time', drop=False)
        del history
        gc.collect()
        for col in ['price', 'origQty', 'executedQty', 'cummulativeQuoteQty',
                    'stopPrice', 'icebergQty', 'origQuoteOrderQty']:
            df[col] = df[col].apply(lambda x: Decimal(x))
        if len(df) > 0 and df.iloc[0].name == start_timestamp:
            # Remove first result when called recursively
            df = df.iloc[1:]

        if len(df) != 0:
            # Get more if available
            last_order_date = Datetime.fromtimestamp(float(df.iloc[-1].name) / 1000)
            time.sleep(1)
            df = df.append(self._get_order_history(symbol=symbol, start_date=last_order_date))
            return df
        else:
            return DataFrame()

    def receiveMsg_MsgGetOrderHistory(self, message: MsgGetOrderHistory, sender: ActorAddress):
        history = self._get_order_history(symbol=message.symbol, start_date=message.start_date)
        self.reply(sender, Response({'orders': history}))

    def receiveMsg_StreamMessage(self, message: StreamMessage, _sender):
        assert type(message.message) == dict
        msg = message.message
        if msg['e'] == 'outboundAccountPosition':
            for asset in msg['B']:
                a = Balance(asset['a'], Decimal(asset['f']), Decimal(asset['l']))
                self.balances[asset['a']] = a

    def receiveMsg_MsgGetBalance(self, message: MsgGetBalance, sender):
        self.reply(sender, Response({'balance': self.balances.get(message.asset_name)}))
