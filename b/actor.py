import tracemalloc
import uuid
from types import GeneratorType
from typing import Tuple, Optional, Dict

from thespian.actors import ActorTypeDispatcher, ActorAddress, PoisonMessage

from b.message import DialogMessage, QuestionBase


class Ask:
    def __init__(self, *, recipient: ActorAddress, message: DialogMessage):
        assert type(recipient) == ActorAddress
        self.recipient = recipient
        assert isinstance(message, DialogMessage)
        self.message = message


class DialoguingActor(ActorTypeDispatcher):
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.coroutines: Dict[int, GeneratorType] = {}
        self.coroutines_questions: Dict[GeneratorType, Tuple[ActorAddress, QuestionBase]] = {}
        self.current_question: Optional[Tuple[ActorAddress, QuestionBase]] = None

    @staticmethod
    def new_message_id():
        return uuid.uuid4()

    def send(self, recipient, message):
        if self.current_question is not None:
            (question_sender, question) = self.current_question
            if recipient == question_sender and isinstance(message, question.answers):
                raise(Exception("use reply to reply to questions"))
        super().send(recipient, message)

    def reply(self, recipient, message):
        assert isinstance(message, DialogMessage)
        if self.current_question is not None:
            (question_sender, question) = self.current_question
            if recipient == question_sender:
                if isinstance(message, question.answers):
                    message.ask_id = question.ask_id
                    super().send(recipient, message)
                else:
                    raise (Exception("Wrong answer to question"))
        else:
            raise (Exception("Replying to no question"))

    def reply_to(self, orig_message, recipient, message):
        assert isinstance(orig_message, DialogMessage)
        assert isinstance(message, DialogMessage)
        message.ask_id = orig_message.ask_id
        self.send(recipient, message)

    def receiveMessage(self, message, sender):
        # Handle responses
        if type(message) == PoisonMessage:
            if hasattr(message.poisonMessage, 'ask_id') and message.poisonMessage.ask_id in self.coroutines:
                # Copy ask_id to poison wrapper and resume processing
                message.ask_id = message.poisonMessage.ask_id
        if hasattr(message, 'ask_id') and message.ask_id in self.coroutines:
            coroutine = self.coroutines[message.ask_id]
            if coroutine in self.coroutines_questions:
                self.current_question = self.coroutines_questions[coroutine]
            del self.coroutines[message.ask_id]
            try:
                coroutine_yield = coroutine.send(message)
                if isinstance(coroutine_yield, Ask):
                    self.coroutines[coroutine_yield.message.ask_id] = coroutine
                    self.send(coroutine_yield.recipient, coroutine_yield.message)
            except StopIteration:
                if coroutine in self.coroutines_questions:
                    del self.coroutines_questions[coroutine]
                pass
            self.current_question = None
        else:
            if isinstance(message, QuestionBase):
                # This is a question
                self.current_question = (sender, message)
            result = super().receiveMessage(message, sender)
            # Handle coroutine message handlers
            if isinstance(result, GeneratorType):
                coroutine = result
                # handler is not a function but a coroutine, run it
                if isinstance(message, QuestionBase):
                    self.coroutines_questions[coroutine] = self.current_question
                try:
                    coroutine_yield = coroutine.send(None)
                    if isinstance(coroutine_yield, Ask):
                        ask_request = coroutine_yield
                        self.coroutines[ask_request.message.ask_id] = coroutine
                        self.send(ask_request.recipient, ask_request.message)
                except StopIteration:
                    pass
                result = None
                self.current_question = None
            return result

