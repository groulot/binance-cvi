from __future__ import annotations

import logging
from typing import Optional

from thespian.actors import ActorTypeDispatcher, ActorAddress

from b.actor import DialoguingActor
from b.message import question, Acknowledge, QuestionBase


class MsgLog:
    def __init__(self, *, message: str):
        assert type(message).__str__ is not object.__str__
        self.message = message


@question(answers=(Acknowledge,))
class MsgSetLogsDestination(QuestionBase):
    def __init__(self, *, destination: ActorAddress):
        super().__init__()
        assert isinstance(destination, ActorAddress)
        self.destination = destination


class AppLog(DialoguingActor):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.log_destination: Optional[ActorAddress] = None

    def receiveMsg_MsgSetLogsDestination(self, message: MsgSetLogsDestination, sender):
        self.log_destination = message.destination
        self.reply(recipient=sender, message=Acknowledge())

    def log(self, message: str):
        assert type(message) == str
        if self.log_destination is None:
            logging.info(message)
        else:
            msg = MsgLog(message=message)
            self.send(self.log_destination, msg)
