from __future__ import annotations

from datetime import timedelta

from thespian.actors import ActorAddress, WakeupMessage

from b.actor import DialoguingActor
from b.message import DialogMessage, Acknowledge


class MsgTimer(DialogMessage):
    def __init__(self, duration: timedelta):
        super().__init__()
        assert type(duration) == timedelta
        self.duration = duration


class Timer(DialoguingActor):
    def receiveMsg_MsgTimer(self, message: MsgTimer, sender: ActorAddress):
        self.wakeupAfter(message.duration, (message, sender))

    def receiveMsg_WakeupMessage(self, message: WakeupMessage, _sender):
        (orig_message, orig_sender) = message.payload
        self.reply_to(orig_message, orig_sender, Acknowledge())
