from __future__ import annotations

from typing import Optional

from thespian.actors import ActorTypeDispatcher, ActorAddress

from actor_phonebook import ActorPhoneBook, Lookup, Register
from b.actor import Ask, DialoguingActor
from b.message import Response


class PhonebookUser(DialoguingActor):
    def phonebook(self):
        return self.createActor(ActorPhoneBook, globalName='phonebook')

    def lookup(self, *, name: str) -> ActorAddress:
        assert type(name) == str
        response = yield Ask(recipient=self.phonebook(), message=Lookup(identifier=name))
        assert type(response) == Response and response.address is not None
        return response.address

    def register(self, *, name: str, address: Optional[ActorAddress] = None):
        assert type(name) == str
        if address is None:
            address = self.myAddress
        assert type(address) == ActorAddress
        response = yield Ask(recipient=self.phonebook(), message=Register(identifier=name, address=address))
        return response