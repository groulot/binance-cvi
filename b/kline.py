import gc
import os
from collections import OrderedDict
from datetime import timedelta, datetime
from decimal import Decimal

from datetimeutc import Datetime
from functools import reduce, lru_cache
from typing import Optional, List, Mapping, Union

import numpy
import pandas
from binance import Client
from pandas import DataFrame
from pyarrow.lib import ArrowInvalid


class Klines:
    class MicroTimestamp(int):
        pass

    class DayTimestamp(int):
        pass

    def __init__(self,
                 *,
                 symbol: str,
                 interval: str,
                 start_date: Optional[Datetime] = None,
                 end_date: Optional[Datetime] = None,
                 columns = None):
        assert type(symbol) == str
        self.symbol = symbol
        assert type(interval) == str and interval in [
            Client.KLINE_INTERVAL_1MINUTE, Client.KLINE_INTERVAL_3MINUTE, Client.KLINE_INTERVAL_5MINUTE,
            Client.KLINE_INTERVAL_15MINUTE, Client.KLINE_INTERVAL_30MINUTE,
            Client.KLINE_INTERVAL_1HOUR, Client.KLINE_INTERVAL_2HOUR, Client.KLINE_INTERVAL_4HOUR,
            Client.KLINE_INTERVAL_6HOUR, Client.KLINE_INTERVAL_8HOUR, Client.KLINE_INTERVAL_12HOUR,
            Client.KLINE_INTERVAL_1DAY, Client.KLINE_INTERVAL_3DAY, Client.KLINE_INTERVAL_1WEEK,
            Client.KLINE_INTERVAL_1MONTH
        ]
        self.interval = interval
        self.columns = columns
        self.data = None
        if start_date is not None:
            if end_date is None:
                end_date = Datetime.now()
            self.load(start_date, end_date, columns)

    @staticmethod
    def from_dict(d: Mapping[str, Union[str, Decimal]]):
        assert {'open_time', 'symbol', 'interval', 'open', 'high', 'low', 'close', 'volume', 'close_time',
                'num_trades', 'closed'}.issubset(set(d.keys()))
        result = Klines(symbol=d['symbol'], interval=d['interval'])
        result.data = pandas.concat([pandas.Series(data=d).to_frame().T.set_index('open_time', drop=False)])
        return result

    @staticmethod
    @lru_cache
    def _parquet_path() -> str:
        return os.path.join(os.getcwd(), 'data', 'spot_klines')

    @staticmethod
    def _setup_df(df: DataFrame, columns):
        types = {
            'open_time': numpy.uint64,
            'symbol': str,
            'interval': str,
            'open': numpy.float64,
            'high': numpy.float64,
            'low': numpy.float64,
            'close': numpy.float64,
            'volume': numpy.float64,
            'close_time': numpy.uint64,
            'quote_volume': numpy.float64,
            'num_trades': numpy.uint64,
            'closed': numpy.bool_}

        if columns is not None:
            types = {k:v for (k, v) in types.items() if k in columns}

        df.astype(
            types,
            copy=False
        )
        df.set_index('open_time', drop=False, inplace=True)
        df['open_day'] = (df['open_time'] / 86400000).round().astype(str)

    @staticmethod
    def _mtimestamp_to_day(t: MicroTimestamp) -> DayTimestamp:
        return Klines.DayTimestamp(t // 86400000)

    @staticmethod
    def _mtimestamp_to_dt(miliseconds: MicroTimestamp) -> Datetime:
        return Datetime.fromtimestamp(int(miliseconds) / 1000)

    @staticmethod
    def _dt_to_mtimestamp(dt: Datetime) -> MicroTimestamp:
        return Klines.MicroTimestamp(dt.timestamp() * 1000)

    def load(self, start_datetime: Datetime, end_datetime: Optional[Datetime], columns=None):
        assert isinstance(start_datetime, Datetime)
        assert end_datetime is None or isinstance(end_datetime, Datetime)
        end_datetime = Datetime.now() if end_datetime is None else end_datetime

        if columns is None:
            columns = self.columns

        dfs: List[DataFrame] = []
        # Step 1, load what's available from disk
        data = self._get_data_from_parquet(start_datetime, end_datetime, columns)

        if data is not None:
            data_start_dt = self._mtimestamp_to_dt(data.iloc[0].open_time)
            dfs = [data]
        else:
            data_start_dt = end_datetime

        if data_start_dt >= start_datetime:
            # We need to load earlier klines from Binance
            earlier_data = self._get_data_from_binance(start_datetime, data_start_dt, columns)
            if earlier_data is not None:
                dfs.insert(0, earlier_data)

        data_end_dt = self._mtimestamp_to_dt(dfs[-1].iloc[-1].open_time)
        if data_end_dt < end_datetime:
            # We need to load later klines from Binance
            later_data = self._get_data_from_binance(data_end_dt, end_datetime, columns)
            if later_data is not None:
                dfs.append(later_data)

        dfs.reverse()
        # Merge dataframes and overwrite data from earlier df with more recent data
        result = reduce(lambda later, earlier: later.combine_first(earlier), dfs)
        result = result.sort_index()
        result = result.drop_duplicates(subset='open_time', keep='last')
        del dfs
        gc.collect()

        self.data = result

    def _get_data_from_parquet(self, start_datetime: Datetime, end_datetime: Datetime, columns) -> Optional[DataFrame]:
        assert isinstance(start_datetime, datetime)
        assert isinstance(end_datetime, datetime)
        if not os.path.exists(self._parquet_path()):
            return None

        # parquet files are split by day
        start_day = str(self._mtimestamp_to_day(
            self._dt_to_mtimestamp(start_datetime.replace(hour=0, minute=0, second=0, microsecond=0))))
        filters = [
            ("symbol", "=", self.symbol),
            ("interval", "=", self.interval),
            ("open_day", ">=", start_day)
        ]
        end_day = str(
            self._mtimestamp_to_day(
                self._dt_to_mtimestamp(end_datetime.replace(hour=23, minute=59, second=59, microsecond=999999))))
        filters.append(("open_day", "<=", end_day))

        try:
            klines = pandas.read_parquet(
                path=self._parquet_path(),
                filters=filters,
                columns=columns
            )
        except ArrowInvalid:
            # Empty parquet
            return None

        if len(klines) == 0:
            return None

        self._setup_df(klines, columns)
        return klines

    def _write_data_to_parquet(self, df: DataFrame):
        # Only write whole days to parquet
        start_datetime: Datetime = self._mtimestamp_to_dt(self.MicroTimestamp(df.iloc[0].open_time))
        if (start_datetime.hour != 0 or start_datetime.minute != 0
                or start_datetime.second != 0 or start_datetime.microsecond != 0):
            # start is not at the beginning or a day, jump to next day
            start_datetime += timedelta(days=1)
            start_datetime = start_datetime.replace(hour=0, minute=0, second=0, microsecond=0)

        end_datetime: Datetime = self._mtimestamp_to_dt(self.MicroTimestamp(int(df.iloc[-1].open_time)))
        if (end_datetime.hour != 23 or end_datetime.minute != 59
                or end_datetime.second != 0 or start_datetime.microsecond != 0):
            # end is not at the last minute of the day, jump to previous day
            end_datetime -= timedelta(days=1)
            end_datetime = end_datetime.replace(hour=23, minute=59, second=0, microsecond=0)

        whole_days_df = df.query(
            "open_time >= %d and open_time <= %d" % (
                self._dt_to_mtimestamp(start_datetime),
                self._dt_to_mtimestamp(end_datetime)))

        whole_days_df.to_parquet(
            engine='pyarrow',
            compression='snappy',
            path=self._parquet_path(),
            partition_cols=['symbol', 'interval', 'open_day'],
            index=False
        )

    def _get_data_from_binance(
            self,
            start_datetime: Datetime,
            end_datetime: Optional[datetime] = None,
            columns = None) -> Optional[DataFrame]:
        assert isinstance(start_datetime, datetime)
        assert isinstance(end_datetime, datetime)
        client = Client("", "")
        end_datetime = Datetime.now() if end_datetime is None else end_datetime
        raw_data = client.get_historical_klines(
            self.symbol,
            self.interval,
            int(self._dt_to_mtimestamp(start_datetime)),
            int(self._dt_to_mtimestamp(end_datetime)))

        if len(raw_data) == 0:
            return None

        const = lambda v: (lambda _: v)
        val = lambda index, fun: (lambda record: fun(record[index]))

        col_processing = {
                'open_time': val(0, numpy.uint64),
                'symbol': const(self.symbol),
                'interval': const(Client.KLINE_INTERVAL_1MINUTE),
                'open': val(1, numpy.float64),
                'high': val(2, numpy.float64),
                'low': val(3, numpy.float64),
                'close': val(4, numpy.float64),
                'volume': val(5, numpy.float64),
                'close_time': val(6, numpy.uint64),
                'quote_volume': val(7, numpy.float64),
                'num_trades': val(8, numpy.uint64),
                'closed': const(True)
            }



        #if columns is None:
        #    processing = col_processing
        #else:
        #    processing = {col: value for (col, value) in col_processing.items() if col in columns}

        dict_data = OrderedDict()
        for k in raw_data:
            dict_data[k[0]] = {}
            for col, p in col_processing.items():
                dict_data[k[0]][col] = p(k)


        df = pandas.DataFrame.from_dict(dict_data, orient='index')
        self._setup_df(df, columns)
        self._write_data_to_parquet(df)

        # Filter columns
        for col in list(df.columns):
            if col not in columns:
                del df[col]

        return df

    def start_time(self) -> datetime:
        return self._mtimestamp_to_dt(self.data.iloc[0].open_time)

    def end_time(self) -> datetime:
        return self._mtimestamp_to_dt(self.data.iloc[-1].open_time)
