from __future__ import annotations

import gc
import json
import os
import queue
import sys
import threading
import time
import uuid
from datetime import timedelta
from multiprocessing import Process, Queue
from typing import Optional
from uuid import UUID

from thespian.actors import ActorAddress, ActorExitRequest
from thespian.initmsgs import initializing_messages
from unicorn_binance_websocket_api import BinanceWebSocketApiManager

from b.actor import DialoguingActor
from b.credentials import Credentials


class Config:
    def __init__(self,
                 *,
                 subscriber: ActorAddress,
                 credentials: Credentials = None,
                 exchange: str = "binance.com",
                 channel: str,
                 market: str
                 ):
        assert type(subscriber) == ActorAddress
        self.subscriber = subscriber
        assert credentials is None or type(credentials) == Credentials
        self.credentials = credentials
        assert isinstance(exchange, str)
        self.exchange = exchange
        assert type(channel) == str
        self.channel = channel
        assert type(market) == str
        self.market = market


class StreamMessage:
    def __init__(self, *, message: dict):
        assert type(message) == dict
        self.message = message


@initializing_messages([('config', Config)], initdone='init_completed')
class Stream(DialoguingActor):
    def __init__(self, *args, **kwargs):
        super(Stream, self).__init__(*args, **kwargs)
        self.manager: Optional[BinanceWebSocketApiManager] = None
        self.stream_id: Optional[UUID] = None
        self.listener: Optional[Process] = None
        self.queue: Queue = Queue()

    def init_completed(self):
        if self.config.credentials is None:
            self.config.credentials = Credentials(api_key="", api_secret="")

        self.listener = Process(target=self.unicorn, args=(self.config, self.queue))
        self.listener.start()
        self.wakeupAfter(timedelta(milliseconds=100.0), payload=None)

    def receiveMsg_WakeupMessage(self, *_):
        try:
            msg = self.queue.get_nowait()
            if 'result' not in msg:
                self.send(self.config.subscriber, StreamMessage(message=msg))
        except queue.Empty:
            pass
        self.wakeupAfter(timedelta(milliseconds=100.0), payload=None)

    @staticmethod
    def unicorn(config, q: Queue):
        try:
            manager = BinanceWebSocketApiManager(exchange=config.exchange, throw_exception_if_unrepairable=True)
            buffer_name = uuid.uuid4().hex
            stream_id = manager.create_stream(
                channels=config.channel,
                markets=config.market,
                api_key=config.credentials.api_key,
                api_secret=config.credentials.api_secret,
                stream_buffer_name=buffer_name
            )
            print("Starting stream %s/%s" % (config.market, config.channel))
            while True:
                if manager.is_manager_stopping():
                    sys.exit(1)
                poll = manager.pop_stream_data_from_stream_buffer(buffer_name)
                if poll:
                    msg = json.loads(poll)
                    if 'result' not in msg:
                        try:
                            q.put_nowait(msg)
                        except queue.Full:
                            pass
                time.sleep(0.1)
        finally:
            manager.stop_stream(stream_id)
            manager.stop_manager_with_all_streams()

    def receiveMsg_ActorExitRequest(self, message: ActorExitRequest, sender):
        #print("%s received exit request" % self.myAddress)
        # Kill child
        self.listener.kill()
        time.sleep(1)
        self.listener.join()
        #print("Binance stream manager stopped")