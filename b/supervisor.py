from __future__ import annotations

from abc import ABC, abstractmethod
from datetime import timedelta
from functools import reduce
from typing import Dict, Iterable, Type, Optional

from thespian.actors import ChildActorExited, ActorAddress, Actor, ActorExitRequest

from b.actor import Ask, DialoguingActor
from b.timer import Timer, MsgTimer


class Supervisor(DialoguingActor, ABC):
    class Child:
        def __init__(self, *, name: str, address: ActorAddress, actor_class: Type[Actor], init_messages: Iterable):
            assert type(name) == str
            self.name = name
            assert type(address) == ActorAddress
            self.address = address
            assert isinstance(init_messages, Iterable)
            self.child_class = actor_class
            self.init_messages = init_messages

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.children: Dict[str, Supervisor.Child] = {}
        self.shutting_down = False

    def create_child(self, *, name: str, actor_class: Type[Actor], init_messages: Iterable):
        assert type(name) == str
        assert issubclass(actor_class, Actor)
        assert isinstance(init_messages, Iterable)
        address = self.createActor(actor_class)
        child = Supervisor.Child(name=name, address=address, actor_class=actor_class, init_messages=init_messages)
        self.children[name] = child
        for message in init_messages:
            self.send(address, message)
        return address

    @abstractmethod
    def child_recreated(self, child):
        pass

    def get_child(self, name: str):
        assert type(name) == str
        return self.children[name].address

    def receiveMsg_ActorExitRequest(self, message: ActorExitRequest, _sender):
        print("ActorExitRequest received in supervisor %s" % self.myAddress)
        # Kill children
        self.shutting_down = True
        message.isRecursive = False
        for _, child in self.children:
            print("Stopping child %s" % child.name)
            self.send(child.address, ActorExitRequest())

    def receiveMsg_ChildActorExited(self, message: ChildActorExited, _sender):
        if self.shutting_down:
            print("%s received ChildActorExited for %s while shutting down" % (self.myAddress, message.childAddress))
            return
        # Wait a bit
        print('create timer %s' % self.shutting_down)
        timer = self.createActor(Timer)
        yield Ask(recipient=timer, message=MsgTimer(duration=timedelta(seconds=10)))
        # Restart the child
        print('restart child')
        child: Optional[Supervisor.Child] = reduce(
            lambda r, c: c[1] if c[1].address == message.childAddress else r,
            self.children.items(),
            None)
        if child is not None:
            del self.children[child.name]
            child.address = self.create_child(
                name=child.name,
                actor_class=child.child_class,
                init_messages=child.init_messages)
            self.child_recreated(child)