class Ticker:
    def __init__(self, symbol, best_bid_price, best_bid_quantity, best_ask_price, best_ask_quantity):
        self.symbol = symbol
        self.best_bid = best_bid_price
        self.best_bid_quantity = best_bid_quantity
        self.best_ask = best_ask_price
        self.best_ask_quantity = best_ask_quantity
