from __future__ import annotations

from typing import Optional

from thespian.actors import ActorTypeDispatcher, ActorAddress

from b.actor import DialoguingActor
from b.message import question, Acknowledge, QuestionBase


class MsgStatus:
    def __init__(self, *, status: Status):
        assert isinstance(status, Status)
        self.status = status


class Status:
    pass


@question(answers=(Acknowledge,))
class MsgSetStatusDestination(QuestionBase):
    def __init__(self, *, destination: ActorAddress):
        super().__init__()
        assert type(destination) == ActorAddress
        self.destination = destination


class AppStatus(DialoguingActor):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.status_destination: Optional[ActorAddress] = None

    def receiveMsg_MsgSetStatusDestination(self, message: MsgSetStatusDestination, sender):
        self.status_destination = message.destination
        self.reply(recipient=sender, message=Acknowledge())

    def send_status(self, status: Status):
        assert isinstance(status, Status)
        if self.status_destination is not None:
            self.send(self.status_destination, MsgStatus(status=status))
