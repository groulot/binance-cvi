from __future__ import annotations


class Exchange:
    def __init__(self, *, exchange: str):
        assert exchange in ["binance.com",
                            "binance.com-testnet",
                            "binance.com-margin",
                            "binance.com-margin-testnet",
                            "binance.com-isolated_margin",
                            "binance.com-isolated_margin-testnet",
                            "binance.com-futures",
                            "binance.com-futures-testnet",
                            "binance.com-coin-futures",
                            "binance.je",
                            "binance.us",
                            "trbinance.com",
                            "jex.com",
                            "binance.org",
                            "binance.org-testnet"]
        self.exchange = exchange
