from __future__ import annotations

import copy
import logging
import timeit
from abc import abstractmethod, ABC
from datetime import timedelta
from decimal import Decimal
from typing import Mapping, Optional, Type, Coroutine, List

import geneticalgorithm2
import numpy
from binance import Client
from datetimeutc import Datetime
from pandas import Series, DataFrame
from thespian.actors import ActorAddress
from thespian.initmsgs import initializing_messages
from to_precision import to_precision

from b.actor import Ask, DialoguingActor
from b.applog import AppLog
from b.appstatus import Status, AppStatus
from b.kline import Klines
from b.message import Response, Acknowledge, MsgInit
from b.spot.market import MsgPlaceOrderResponse, MsgPlaceOrder, MsgExecutionReport
from b.phonebook_user import PhonebookUser
from b.spot.account import Balance, MsgGetOrderHistory, MsgGetBalance
from b.spot.symbol import MsgKLine, MsgGetBaseAsset, MsgGetQuoteAsset, MsgSubscribeKLines, MsgGetKLines, \
    MsgGetSymbolFilter
from b.spot.symbol_filter import SymbolFilter
from b.spot_utils import position_from_order_list, drop_obsolete_orders, get_first_position

python_type = type


def _format_price(f):
    return to_precision(f, 6, 'std', strip_zeros=True, preserve_integer=True)


class Config:
    def __init__(self,
                 *,
                 symbol_name: str,
                 fund: Decimal,
                 worth_target: Decimal,
                 fee: Decimal = Decimal('0.00075')):
        assert type(symbol_name) == str
        self.symbol = symbol_name
        assert type(fund) == Decimal and fund >= Decimal(0)
        self.fund = fund
        assert type(worth_target) == Decimal
        self.worth_target = worth_target
        assert type(fee) == Decimal and fee >= Decimal(0)
        self.fee = fee


class Strategy(ABC):
    class PerformanceType(ABC):
        def __init__(self, perf: Decimal):
            assert type(perf) == Decimal
            self.perf = perf * Decimal('100')

        @abstractmethod
        def __str__(self):
            pass

    class PerformanceTypeVsHold(PerformanceType):
        def __str__(self):
            return "VS Hold: {:.2f}%".format(self.perf)

    class PerformanceTypeAfterSwing(PerformanceType):
        def __str__(self):
            return "VS swing to same price: {:.2f}%".format(self.perf)

    class Request:
        pass

    class AskKLine(Request):
        pass

    class AskFund(Request):
        pass

    class AskFee(Request):
        pass

    class AskBalance(Request):
        pass

    class AskOrderHistory(Request):
        def __init__(self, *, start_date: Datetime):
            assert isinstance(start_date, Datetime)
            self.start_date = start_date

    class AskKlineHistory(Request):
        def __init__(self, *, start_date: Datetime, columns=None):
            assert isinstance(start_date, Datetime)
            self.start_date = start_date
            self.columns = columns

    class AskSymbolFilter(Request):
        pass

    class PlaceOrder(Request):
        def __init__(self, *, side, quote_value):
            assert side in [Client.SIDE_SELL, Client.SIDE_BUY]
            self.side = side
            assert type(quote_value) == Decimal and quote_value > Decimal(0)
            self.quote_value = quote_value

    class Log(Request):
        def __init__(self, message):
            assert type(message).__str__ is not object.__str__
            self.message = message

    class Status(Request):
        def __init__(self, status):
            assert isinstance(status, Status)
            self.status = status

    class Response:
        pass

    class KLine(Response):
        def __init__(self, *, kline: Series):
            assert type(kline) == Series
            self.kline = kline

    # class NewBalances(Response):
    #    def __init__(self, *, base_asset, quote_asset):
    #        assert type(base_asset) == Decimal and base_asset > Decimal(0)
    #        self.base_asset = base_asset
    #        assert type(quote_asset) == Decimal and quote_asset > Decimal(0)
    #        self.quote_asset = quote_asset

    class OrderStatus(Response):
        def __init__(self,
                     *,
                     side: str,
                     type: str,
                     clientOrderId: str,
                     status: str,
                     cumulativeFilledQty: Decimal,
                     cumulativeQuoteAssetTransactedQty: Decimal,
                     date: Datetime):
            assert side in [Client.SIDE_SELL, Client.SIDE_BUY]
            self.side = side
            assert type in [Client.ORDER_TYPE_LIMIT,
                            Client.ORDER_TYPE_MARKET,
                            Client.ORDER_TYPE_STOP_LOSS,
                            Client.ORDER_TYPE_STOP_LOSS_LIMIT,
                            Client.ORDER_TYPE_TAKE_PROFIT,
                            Client.ORDER_TYPE_TAKE_PROFIT_LIMIT,
                            Client.ORDER_TYPE_LIMIT_MAKER]
            self.type = type
            assert python_type(clientOrderId) == str
            self.clientOrderId = clientOrderId
            assert status in [
                Client.ORDER_STATUS_NEW,
                Client.ORDER_STATUS_PARTIALLY_FILLED,
                Client.ORDER_STATUS_FILLED,
                Client.ORDER_STATUS_CANCELED,
                Client.ORDER_STATUS_PENDING_CANCEL,
                Client.ORDER_STATUS_REJECTED,
                Client.ORDER_STATUS_EXPIRED]
            self.status = status
            assert python_type(cumulativeFilledQty) == Decimal
            self.cumulativeFilledQty = cumulativeFilledQty
            assert python_type(cumulativeQuoteAssetTransactedQty) == Decimal
            self.cumulativeQuoteAssetTransactedQty = cumulativeQuoteAssetTransactedQty
            assert isinstance(date, Datetime)
            self.date = date

    @abstractmethod
    def run(self):
        pass

    @abstractmethod
    def get_performance(self) -> Mapping[PerformanceType, Decimal]:
        pass


class TraillingStop:
    def __init__(self, *, price: Decimal, percent: Decimal = Decimal('1.5'), trailling_percent: Decimal = Decimal(10)):
        assert type(price) == Decimal
        self.start_price: Decimal = price
        assert type(percent) == Decimal
        self.percent = percent / Decimal(100)
        assert type(trailling_percent) == Decimal
        self.trailling_percent = trailling_percent / Decimal(100)
        self.max: Decimal = price
        self.min: Decimal = price
        self.prices: List[Decimal] = []

    def need_rebalance(self, *, price: Decimal) -> bool:
        assert type(price) == Decimal
        self.prices.append(price)
        if len(self.prices) > 30:
            self.prices = self.prices[-30:]
        smooth_price = sum(self.prices) / len(self.prices)
        return self.need_rebalance2(price=smooth_price) and self.need_rebalance2(price=price)

    def need_rebalance2(self, *, price: Decimal) -> bool:
        self.max = max(self.max, price)
        self.min = min(self.min, price)
        if price / self.start_price < 1 + self.percent and price / self.start_price > 1 - self.percent:
            return False
        if price > self.start_price and price < self.max - (self.max - self.start_price) * self.trailling_percent:
            return True
        if price < self.start_price and price > self.min + (self.start_price - self.min) * self.trailling_percent:
            return True
        return False


class ConstantValueStrategy(Strategy):
    class Status(Status):
        def __init__(self,
                     *,
                     trailling_stop: TraillingStop,
                     coin: Decimal,
                     fund: Decimal,
                     price: Decimal,
                     initial_price: Decimal,
                     initial_date: Datetime,
                     initial_coin: Decimal,
                     breakeven: Decimal,
                     rebalances: int,
                     klines: DataFrame,
                     orders: DataFrame
                     ):
            assert type(trailling_stop) == TraillingStop
            self.trailling_stop = trailling_stop
            assert type(coin) == Decimal
            self.coin = coin
            assert type(fund) == Decimal
            self.fund = fund
            assert type(price) == Decimal
            self.price = price
            assert type(initial_price) == Decimal
            self.initial_price = initial_price
            assert isinstance(initial_date, Datetime)
            self.initial_date = initial_date
            assert type(initial_coin) == Decimal
            self.initial_coin = initial_coin
            assert type(breakeven) == Decimal
            self.breakeven = breakeven
            assert type(rebalances) == int
            self.rebalances = rebalances
            assert type(klines) == DataFrame
            self.klines = klines
            assert type(orders) == DataFrame
            self.orders = orders

        def __str__(self):
            msg = ["First buy at {} with price {:8.8f}".format(self.initial_date, self.initial_price),
                   "Coin worth: {:8.8f}".format(self.coin * self.price),
                   "Total worth: {:8.8f}".format(self.coin * self.price + self.fund),
                   "Breakeven: {:8.8f}".format(self.breakeven)]
            hworth = self.price * self.initial_coin * 2
            if hworth != Decimal(0):
                msg.append("Comparative HODL worth: {:8.8f} ({:3.4f}%)".format(
                    hworth,
                    Decimal(100) * (self.coin * self.price + self.fund) / hworth))
            msg += ["Coins: {:8.8f}".format(self.coin),
                    "Funds: {:8.8f}".format(self.fund),
                    "Rebalances: {}".format(self.rebalances),
                    "Money if back to first price : {:8.8f}".format(
                        (self.coin * self.initial_price + self.fund))]
            return "\n".join(msg)

    def __init__(self, *, worth_target: Decimal, start_date: Optional[Datetime] = None):
        super().__init__()
        assert type(worth_target) == Decimal
        self.worth_target = worth_target
        assert start_date is None or isinstance(start_date, Datetime)
        self.start_date = Datetime(year=2001, month=1, day=1) if start_date is None else start_date

        self.display = False
        self.rebalance_percent_threshold: Decimal = Decimal('0.005')
        self.rebalance_min_quote_amount: Decimal = Decimal(20)
        self.klines: DataFrame = DataFrame(columns=['open_time', 'close']) \
            .set_index('open_time', drop=False) \
            .astype(dtype=float)

    def log(self, message):
        yield Strategy.Log(message=message)

    def status(self, status):
        yield Strategy.Status(status=status)

    def run(self) -> Coroutine[Strategy.Request, Series, None]:
        # Ask for provided funds
        fund = yield Strategy.AskFund()
        assert type(fund) == Decimal
        self.start_fund: Decimal = fund
        self.fund: Decimal = fund

        # Ask for broker fee
        fee = yield Strategy.AskFee()
        assert type(fee) == Decimal
        self.fee = fee

        # Ask for balance
        balance = yield Strategy.AskBalance()
        assert type(balance) == Balance
        self.coin: Decimal = balance.free
        self.nb_rebalances: int = 0

        # Ask for order history
        self.order_history = yield Strategy.AskOrderHistory(start_date=self.start_date)
        self.order_history = self.order_history.query("status == 'FILLED'")

        # Ask for Klines
        kline_history = yield Strategy.AskKlineHistory(start_date=self.start_date, columns=['open_time', 'close'])
        self.klines = self.klines.append(kline_history)

        # Remove old history dating from before a return to 0 asset
        self.order_history = drop_obsolete_orders(self.order_history)

        # Get starting position
        position = get_first_position(orders=self.order_history, worth_target=self.worth_target)

        self.initial_breakeven_price: Decimal = position.price
        self.initial_breakeven_coin: Decimal = position.quantity
        self.initial_time: Datetime = position.date
        self.initial_fund: Decimal = self.fund - self.initial_breakeven_price * self.initial_breakeven_coin

        # Get current position
        position = position_from_order_list(orders=self.order_history)
        self.fund -= position.price * position.quantity
        self.breakeven = position.price

        symbol_filter = yield Strategy.AskSymbolFilter()
        assert type(symbol_filter) == SymbolFilter
        self.symbol_filter: SymbolFilter = symbol_filter
        #self.sma9: float = 0.0
        #self.sma20: float = 0.0
        self.kline: Optional[Series] = None
        self.last_summary = Datetime.now() - timedelta(days=1)
        self.response = yield Strategy.AskKLine()

        # Configure trailling stop with last trade's price
        if len(self.order_history) > 0:
            last_order = self.order_history.iloc[-1]
            last_price = Decimal(last_order['cummulativeQuoteQty']) / Decimal(last_order['executedQty'])
            self.trailling_stop = TraillingStop(price=last_price)
        else:
            self.trailling_stop = TraillingStop(price=Decimal(self.response.kline.close))
        yield from self.log(message="Trailling stop set at: {:8.8f}".format(self.trailling_stop.start_price))

        self.waiting_on_order = False
        while True:
            if type(self.response) == Strategy.KLine:
                yield from self.on_kline(self.response.kline)
            # if type(self.response) == Strategy.NewBalances:
            #    new_balance: Strategy.NewBalances = self.response
            #    self.coin = new_balance.base_asset
            #    self.fund = new_balance.quote_asset
            #    yield from self._summary(self.kline)
            #    self.last_summary = Datetime.now()
            elif type(self.response) == Strategy.OrderStatus:
                order_status: Strategy.OrderStatus = self.response
                yield from self.log("Order status received: %s" % order_status)
                if order_status.status == Client.ORDER_STATUS_FILLED:
                    self.waiting_on_order = False
                    order = {
                        'cummulativeQuoteQty': order_status.cumulativeQuoteAssetTransactedQty,
                        'executedQty': order_status.cumulativeFilledQty,
                        'side': order_status.side,
                        'type': order_status.type,
                        'status': order_status.status,
                        'time': int(order_status.date.timestamp() * 1000)
                    }
                    self.order_history.append(Series(data=order, name=order['time']))
                    self.breakeven = position_from_order_list(orders=self.order_history).price
                    if order_status.side == Client.SIDE_SELL:
                        self.coin -= order_status.cumulativeFilledQty
                        self.fund += order_status.cumulativeQuoteAssetTransactedQty
                    elif order_status.side == Client.SIDE_BUY:
                        self.coin += order_status.cumulativeFilledQty
                        self.fund -= order_status.cumulativeQuoteAssetTransactedQty
                    yield from self._summary(self.kline)

            self.response = yield Strategy.AskKLine()

            if Datetime.now() - self.last_summary > timedelta(minutes=60):
                yield from self._summary(self.kline)
                self.last_summary = Datetime.now()
                self.response = yield Strategy.AskKLine()

    def worth(self, price: Decimal):
        return self.coin * price

    def _rebalance(self, *, worth_diff: Decimal, kline: Series):
        close = Decimal(kline.close)
        self.nb_rebalances += 1
        if worth_diff > Decimal(0):
            # Need to sell
            self.waiting_on_order = True
            if self.display:
                yield from self.log("Selling base asset around price {:8.8f} for {:8.8f} quote asset".format(
                    close,
                    worth_diff
                ))
            self.response = yield Strategy.PlaceOrder(
                side=Client.SIDE_SELL,
                quote_value=self.symbol_filter.filter_quote_order_quantity(quantity=worth_diff))
        elif worth_diff <= Decimal(0) and self.fund > abs(worth_diff):
            # Need to buy
            self.waiting_on_order = True
            if self.display:
                yield from self.log("Buying base asset around price {:8.8f} for {:8.8f} quote asset".format(
                    close,
                    -worth_diff
                ))
            self.response = yield Strategy.PlaceOrder(
                side=Client.SIDE_BUY,
                quote_value=self.symbol_filter.filter_quote_order_quantity(quantity=-worth_diff))
            if self.initial_breakeven_price == Decimal(0):
                self.initial_breakeven_price = close

    def _status(self, kline: Series):
        return ConstantValueStrategy.Status(
            trailling_stop=self.trailling_stop,
            coin=self.coin,
            fund=self.fund,
            price=Decimal(kline.close),
            initial_price=self.initial_breakeven_price,
            initial_date=self.initial_time,
            initial_coin=self.initial_breakeven_coin,
            breakeven=self.breakeven,
            rebalances=self.nb_rebalances,
            klines=self.klines,
            orders=self.order_history
        )

    def _summary(self, kline: Series):
        if not self.display:
            return
        status = self._status(kline)
        yield from self.log(message=status)

    def on_kline(self, kline: Series):
        self.klines.loc[kline.name] = kline
        # logging.info("kline: %s" % kline.close)
        self.kline = kline
        close = Decimal(kline.close)
        worth = self.worth(close)
        worth_diff = worth - self.worth_target
        if self.initial_breakeven_price == Decimal(0):
            yield from self._rebalance(worth_diff=worth_diff, kline=kline)
            yield from self._summary(kline)
            return
        # if ((kline.SMA9 < kline.SMA20 and self.sma9 > self.sma20)
        #    or (kline.SMA9 > kline.SMA20 and self.sma9 < self.sma20)
        #    ) and abs(worth_diff) > self.worth_target * Decimal('0.02') and not self.waiting_on_order \
        #        and abs(worth_diff) > self.symbol_filter.min_notional:
        #    print('SMA crossing')
        #    yield from self._rebalance(worth_diff=worth_diff, kline=kline)
        # if not self.waiting_on_order and abs(worth_diff) > self.worth_target * Decimal('0.05') \
        #        and abs(worth_diff) > self.symbol_filter.min_notional:
        #    print('Worth diff > 5%')
        #    yield from self._rebalance(worth_diff=worth_diff, kline=kline)
        if self.trailling_stop.need_rebalance(price=close) and abs(worth_diff) > self.symbol_filter.min_notional:
            self.trailling_stop = TraillingStop(price=close)
            yield from self._rebalance(worth_diff=worth_diff, kline=kline)
        #self.sma9 = kline.SMA9
        #self.sma20 = kline.SMA20
        status = self._status(kline)
        yield from self.status(status=status)

    def get_performance(self) -> Mapping[Type[Strategy.PerformanceType], Strategy.PerformanceType]:
        holdWorth = self.start_fund * Decimal(self.kline.close) / self.initial_breakeven_price
        ourWorth = self.worth(Decimal(self.kline.close)) + self.fund
        ourWorthStartPrice = self.worth((self.initial_breakeven_price)) + self.fund
        return {
            Strategy.PerformanceTypeVsHold: Strategy.PerformanceTypeVsHold(ourWorth / holdWorth),
            Strategy.PerformanceTypeAfterSwing: Strategy.PerformanceTypeAfterSwing(ourWorthStartPrice / self.start_fund)
        }


@initializing_messages([('config', Config), ('strategy', Strategy)])
class StrategyRunnerBase(PhonebookUser, DialoguingActor):
    def receiveMsg_MsgInit(self, _message: MsgInit, sender):
        yield from self.do_init()
        self.reply(sender, Acknowledge())

    def do_init(self):
        self.symbol_name: str = self.config.symbol

        old_factory = logging.getLogRecordFactory()

        def record_factory(*args, **kwargs):
            record = old_factory(*args, **kwargs)
            record.actorName = self.symbol_name
            return record

        logging.setLogRecordFactory(record_factory)

        self.fund: Decimal = self.config.fund
        self.fee: Decimal = self.config.fee
        # lookup the symbol's actor
        self.symbol: ActorAddress = yield from self.lookup(name='symbol_{}'.format(self.symbol_name.lower()))

        response2 = yield Ask(recipient=self.symbol, message=MsgGetBaseAsset())
        self.base_asset = response2.base_asset
        response3 = yield Ask(recipient=self.symbol, message=MsgGetQuoteAsset())
        self.quote_asset = response3.quote_asset
        response4 = yield Ask(
            recipient=self.symbol,
            message=MsgGetSymbolFilter(symbol=self.symbol_name))
        self.symbol_filter = response4.symbol_filter


class StrategyTester(StrategyRunnerBase):
    class Optimize:
        pass

    class RunOnHistory:
        pass

    def do_init(self):
        yield from super().do_init()

    def receiveMsg_Optimize(self, _message: Optimize, _sender):
        yield from self.optimize_strategy()
        logging.info("Optimization completed.")

    def optimize_strategy(self):
        response = yield Ask(recipient=self.symbol, message=MsgGetKLines())
        assert type(response) == Response and type(response.kline) == Klines
        klines = response.kline
        var_bounds = numpy.array([[0.0, 0.1], [0.0, 100.0]])

        def f(x):
            return -float(self.run_strategy(klines, x[0], x[1]))

        model = geneticalgorithm2.geneticalgorithm2(
            f,
            dimension=2,
            variable_type='real',
            variable_boundaries=var_bounds,
            variable_type_mixed=None,
            function_timeout=10,
            algorithm_parameters={'max_num_iteration': None,
                                  'population_size': 100,
                                  'mutation_probability': 0.1,
                                  'elit_ratio': 0.01,
                                  'crossover_probability': 0.5,
                                  'parents_portion': 0.3,
                                  'crossover_type': 'uniform',
                                  'mutation_type': 'uniform_by_center',
                                  'selection_type': 'roulette',
                                  'max_iteration_without_improv': None}
        )

        model.run(
            no_plot=False,
            disable_progress_bar=False,
            set_function=None,
            apply_function_to_parents=False,
            start_generation={'variables': None, 'scores': None},
            studEA=False,
            mutation_indexes=None,
            init_creator=None,
            init_oppositors=None,
            duplicates_oppositor=None,
            remove_duplicates_generation_step=None,
            revolution_oppositor=None,
            revolution_after_stagnation_step=None,
            revolution_part=0.3,
            population_initializer=geneticalgorithm2.Population_initializer(
                select_best_of=1,
                local_optimization_step='never',
                local_optimizer=None),
            stop_when_reached=None,
            callbacks=[],
            middle_callbacks=[],
            time_limit_secs=None,
            save_last_generation_as=None,
            seed=None
        )

    def receiveMsg_RunOnHistory(self, _message: RunOnHistory, _sender):
        yield from self.run_on_history()
        logging.info("Run completed.")

    def run_on_history(self):
        response = yield Ask(recipient=self.symbol, message=MsgGetKLines())
        assert type(response) == Response and type(response.kline) == Klines
        klines = response.kline
        self.run_strategy(klines, display=True)

    def run_strategy(self, klines, a=None, b=None, display=False) -> Decimal:
        strategy = copy.deepcopy(self.strategy)
        if a is not None:
            strategy.rebalance_percent_threshold = Decimal(a)
        if b is not None:
            strategy.rebalance_min_quote_amount = Decimal(b)
        current_kline = 0
        start_time = timeit.default_timer()
        coro = strategy.run()
        reply_to_coro = None
        while True and current_kline < len(klines.data):
            # Process and get desired action, give reply to previous action too
            action = coro.send(reply_to_coro)
            if type(action) == Strategy.AskKLine:
                reply_to_coro = klines.data.iloc[current_kline]
                current_kline += 1
            if type(action) == Strategy.AskFund:
                reply_to_coro = self.fund
            if type(action) == Strategy.AskFee:
                reply_to_coro = self.fee

        duration = timeit.default_timer() - start_time
        perfs: Mapping[Type[Strategy.PerformanceType], Strategy.PerformanceType] = strategy.get_performance()
        if display:
            logging.info("Processing {} candles done in {}".format(len(klines.data), duration))
            logging.info('Performance:')
            for p in perfs.values():
                logging.info(p)
        return perfs[Strategy.PerformanceTypeAfterSwing].perf


class StrategyRunner(AppStatus, AppLog, StrategyRunnerBase):
    class Run:
        pass

    def log(self, message: str):
        message = "%s bot: %s" % (self.symbol_name, message)
        super().log(message)

    def do_init(self):
        yield from super().do_init()
        self.log("Initialising...")
        self.account: ActorAddress = yield from self.lookup(name='account')
        self.market: ActorAddress = yield from self.lookup(name='market_%s' % self.symbol_name.lower())

        response = yield Ask(recipient=self.account, message=MsgGetBalance(asset_name=self.base_asset))
        assert type(response) == Response and response.balance is not None
        self.balance = response.balance

        # Set up strategy coroutine
        self.strategy.display = True
        self.coro = self.strategy.run()
        self.action = None
        self.last_kline = None

    def receiveMsg_Run(self, _message: Run, _sender):
        self.log("Starting...")
        # Subscribe to KLines
        self.send(self.symbol, MsgSubscribeKLines())
        # Run strategy
        yield from self.run_strategy()

    def run_strategy(self, reply=None):
        self.action = self.coro.send(reply)
        # Process and get desired action, give reply to previous action too
        if type(self.action) == Strategy.AskFund:
            yield from self.run_strategy(self.fund)
        if type(self.action) == Strategy.AskFee:
            yield from self.run_strategy(self.fee)
        if type(self.action) == Strategy.AskBalance:
            yield from self.run_strategy(self.balance)
        if type(self.action) == Strategy.AskOrderHistory:
            responce = yield Ask(
                recipient=self.account,
                message=MsgGetOrderHistory(symbol=self.symbol_name, start_date=self.action.start_date))
            assert type(responce) == Response
            yield from self.run_strategy(responce.orders)
        if type(self.action) == Strategy.AskKlineHistory:
            responce = yield Ask(
                recipient=self.symbol,
                message=MsgGetKLines(
                    start_datetime=self.action.start_date,
                    columns=self.action.columns))
            assert type(responce) == Response
            yield from self.run_strategy(responce.kline)
        if type(self.action) == Strategy.AskSymbolFilter:
            yield from self.run_strategy(self.symbol_filter)
        if type(self.action) == Strategy.PlaceOrder:
            order: Strategy.PlaceOrder = self.action
            response = yield Ask(recipient=self.market, message=MsgPlaceOrder(
                symbol=self.symbol_name,
                side=order.side,
                quoteOrderQty=order.quote_value
            ))
            assert type(response) == MsgPlaceOrderResponse
            yield from self.run_strategy(Strategy.OrderStatus(
                side=response.side,
                type=response.type,
                status=Client.ORDER_STATUS_NEW,
                clientOrderId=response.clientOrderId,
                cumulativeFilledQty=Decimal(0),
                cumulativeQuoteAssetTransactedQty=Decimal(0),
                date=response.transactTime
            ))
        if type(self.action) == Strategy.Log:
            log: Strategy.Log = self.action
            self.log(log.message)
            yield from self.run_strategy(None)
        if type(self.action) == Strategy.Status:
            status: Strategy.Status = self.action
            self.send_status(status.status)
            yield from self.run_strategy(None)
        assert type(self.action) == Strategy.AskKLine, "Action is %s" % self.action

    def receiveMsg_MsgKLine(self, msg: MsgKLine, _sender):
        if type(self.action) == Strategy.AskKLine:
            self.last_kline = msg.kline
            yield from self.run_strategy(Strategy.KLine(kline=msg.kline))

    def receiveMsg_MsgExecutionReport(self, msg: MsgExecutionReport, _sender):
        yield from self.run_strategy(Strategy.OrderStatus(
            side=msg.side,
            type=msg.type,
            status=msg.status,
            clientOrderId=msg.clientOrderId,
            cumulativeFilledQty=msg.cumulativeFilledQty,
            cumulativeQuoteAssetTransactedQty=msg.cumulativeQuoteAssetTransactedQty,
            date=msg.transactionTime
        ))
