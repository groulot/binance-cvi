import random

from kivy.app import App
from kivy.clock import Clock
from kivy.graphics.transformation import Matrix
from kivy.uix.scatterlayout import ScatterLayout
from kivy.uix.widget import Widget
from kivy.graphics import Color, Ellipse
from math import sin
from kivy.garden.graph import Graph, MeshLinePlot

class MyPaintWidget(Widget):
    def on_touch_down(self, touch):
        with self.canvas:
            Color(1, 1, 0)
            d = 30.
            Ellipse(pos=(touch.x - d/2, touch.y - d/2), size=(d, d))


class TestApp(App):
    def update(self, dt):
        plot = MeshLinePlot(color=[1, 0, 0, 1])
        r = random.random()
        #plot.points = [(x, r * sin(x / 10.)) for x in range(0, 101)]
        mat = Matrix().scale(r, r, r)
        self.scatter.transform = mat
        self.graph.add_plot(plot)

    def build(self):
        self.graph = Graph(xlabel='X', ylabel='Y', x_ticks_minor=5, x_ticks_major=25, y_ticks_major=1, y_grid_label=True,
                      x_grid_label=True, padding=5, x_grid=True, y_grid=True, xmin=-0, xmax=100, ymin=-1, ymax=1)
        plot = MeshLinePlot(color=[1, 0, 0, 1])
        plot.points = [(x, sin(x / 10.)) for x in range(0, 101)]
        self.graph.add_plot(plot)
        Clock.schedule_interval(self.update, 1)
        self.scatter = ScatterLayout(
            do_rotation=False,
            do_scale=False
        )
        self.scatter.add_widget(self.graph)
        return self.scatter


i = TestApp()
i.run()