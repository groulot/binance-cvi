from __future__ import annotations

import pickle
import re
import time
import tracemalloc
from datetime import timedelta
from decimal import Decimal
from functools import reduce
from unittest import TestCase
from unittest.mock import Mock

import psutil
from binance import Client
from datetimeutc import Datetime
from pandas import DataFrame, Series
from thespian.actors import ActorSystem, ActorAddress, Actor, ActorTypeDispatcher, ActorExitRequest
from thespian.initmsgs import initializing_messages

from actor_phonebook import ActorPhoneBook, Register, Lookup
from b import stream
from b.actor import Ask, DialoguingActor
from b.applog import AppLog, MsgSetLogsDestination
from b.appstatus import AppStatus, Status, MsgSetStatusDestination
from b.credentials import Credentials
from b.exchange import Exchange
from b.kline import Klines
from b.message import Acknowledge, Response, Error, DialogMessage
from b.phonebook_user import PhonebookUser
from b.spot.symbol import Symbol, MsgGetKLines
from b.stream import Stream, Config
from b.supervisor import Supervisor
from b.timer import Timer, MsgTimer
from b.spot_utils import position_from_order_list, drop_obsolete_orders, set_sign_according_to_side, get_first_position


class TestUtils(TestCase):
    def test_set_sign_according_to_side(self):
        orders = DataFrame.from_dict({
            'status': ['FILLED', 'FILLED', 'UNFILLED', 'FILLED'],
            'side': ['BUY', 'BUY', 'BUY', 'SELL'],
            'executedQty': [Decimal(1), Decimal(1), Decimal(0.1), Decimal('0.5')],
            'cummulativeQuoteQty': [Decimal(100), Decimal(50), Decimal(5), Decimal(75)],
            'time': [1623502891000, 1623502892000, 1623502892500, 1623502893000]
        })
        orders.set_index('time', drop=False)
        orders = set_sign_according_to_side(orders)
        self.assertTrue(orders.iloc[0].executedQty > Decimal(0))
        self.assertTrue(orders.iloc[0].cummulativeQuoteQty > Decimal(0))
        self.assertTrue(orders.iloc[-1].executedQty < Decimal(0))
        self.assertTrue(orders.iloc[-1].cummulativeQuoteQty < Decimal(0))

    def test_position_from_order_list(self):
        orders = DataFrame.from_dict({
            'status': ['FILLED', 'FILLED', 'UNFILLED', 'FILLED'],
            'side': ['BUY', 'BUY', 'BUY', 'SELL'],
            'executedQty': [Decimal(1), Decimal(1), Decimal(0.1), Decimal('0.5')],
            'cummulativeQuoteQty': [Decimal(100), Decimal(50), Decimal(5), Decimal(75)],
            'time': [1623502891000, 1623502892000, 1623502892500, 1623502893000]
        })
        orders.set_index('time', drop=False)
        position = position_from_order_list(orders=orders)
        self.assertEqual(position.price, Decimal(50))

    def test_drop_obsolete_orders(self):
        no_obsolete = DataFrame.from_dict({
            'status': ['FILLED', 'UNFILLED', 'FILLED', 'FILLED', 'FILLED', 'FILLED', 'FILLED'],
            'side': ['BUY', 'BUY', 'BUY', 'SELL', 'SELL', 'BUY', 'BUY'],
            'executedQty': [Decimal(1), Decimal(1), Decimal(1), Decimal('0.5'), Decimal('0.1'), Decimal('0.1'), Decimal('0.33')],
            'cummulativeQuoteQty': [Decimal(100), Decimal(105), Decimal(50), Decimal(75), Decimal('133'), Decimal(12), Decimal(13)],
            'time': [1623502890001, 1623502890001, 1623502890002, 1623502890003, 1623502890004, 1623502890005, 1623502890006]
        }).set_index('time', drop=False)
        obsolete = DataFrame.from_dict({
            'status': ['FILLED', 'UNFILLED', 'FILLED', 'FILLED', 'FILLED', 'FILLED', 'FILLED'],
            'side': ['BUY', 'BUY', 'BUY', 'SELL', 'SELL', 'BUY', 'BUY'],
            'executedQty': [Decimal(1), Decimal(1), Decimal(1), Decimal('0.5'), Decimal('1.5'), Decimal('0.1'), Decimal('0.33')],
            'cummulativeQuoteQty': [Decimal(100), Decimal(105), Decimal(50), Decimal(75), Decimal('133'), Decimal(12), Decimal(13)],
            'time': [1623502890001, 1623502890001, 1623502890002, 1623502890003, 1623502890004, 1623502890005, 1623502890006]
        }).set_index('time', drop=False)

        result = drop_obsolete_orders(orders=no_obsolete)
        self.assertTrue(no_obsolete.equals(result))

        result = drop_obsolete_orders(orders=obsolete)
        self.assertFalse(obsolete.equals(result))

    def test_get_first_position(self):
        orders = DataFrame.from_dict({
            'status': ['FILLED', 'UNFILLED', 'FILLED', 'FILLED', 'FILLED', 'FILLED', 'FILLED'],
            'side': ['BUY', 'SELL', 'SELL', 'BUY', 'BUY', 'BUY', 'SELL'],
            'executedQty': [Decimal(1), Decimal(1), Decimal(1), Decimal('0.5'), Decimal('0.2'), Decimal('0.1'),
                            Decimal('0.33')],
            'cummulativeQuoteQty': [Decimal(100), Decimal(105), Decimal(50), Decimal(75), Decimal('133'), Decimal(12),
                                    Decimal(13)],
            'time': [1623502891000, 1623502891500, 1623502892000, 1623502893000, 1623502894000, 1623502895000,
                     1623502896000]
        }).set_index('time', drop=False)

        result = get_first_position(orders=orders, worth_target=Decimal('465.5'))
        self.assertEqual(result.quantity, Decimal('0.7'))
        self.assertEqual(result.date, Datetime(year=2021, month=6, day=12, hour=13, minute=1, second=34))


class TestPhonebook(TestCase):
    def test(self):
        try:
            asys = ActorSystem(systemBase='multiprocQueueBase')
            phonebook = asys.createActor(ActorPhoneBook, globalName='phonebook')

            with self.subTest('Register'):
                result = asys.ask(actorAddr=phonebook, msg=Register(identifier='bob', address=phonebook))
                self.assertIsInstance(result, Acknowledge)

            with self.subTest('Lookup existing'):
                result = asys.ask(actorAddr=phonebook, msg=Lookup(identifier='bob'))
                self.assertIsInstance(result, Response)
                self.assertEqual(result.address, phonebook)

            with self.subTest('Lookup nonexisting'):
                result = asys.ask(actorAddr=phonebook, msg=Lookup(identifier='bobibob'))
                self.assertIsInstance(result, Error)
        finally:
            asys.shutdown()


class Eval:
    def __init__(self, path, args, kwargs):
        self.path = path
        self.args = args
        self.kwargs = kwargs

class MockActor(ActorTypeDispatcher):
    def receiveMsg_Eval(self, msg: Eval, sender):
        try:
            attr = reduce(lambda inst, item: inst.__getattribute__(item), msg.path, self)
            result = attr(*msg.args, **msg.kwargs)
            self.send(targetAddr=sender, msg=result)
        except Exception as ex:
            self.send(targetAddr=sender, msg=ex)

    def receiveUnrecognizedMessage(self, msg, sender):
        attr_name = 'receiveMsg_%s' % type(msg).__name__
        self.__setattr__(attr_name, Mock())
        self.__getattribute__(attr_name)(self, msg, sender)

class ActorProxy:
    def __init__(self, asys, actor, attribute_path = []):
        self.asys = asys
        self.actor = actor
        self.attribute_path = attribute_path

    def __getattr__(self, item):
        return ActorProxy(self.asys, self.actor, self.attribute_path + [item])

    def __call__(self, *args, **kwargs):
        result = self.asys.ask(self.actor, Eval(self.attribute_path, args, kwargs))
        if isinstance(result, Exception):
            raise result

class LoggingActor(AppLog):
    def receiveMsg_str(self, message, _sender):
        self.log(message)


class TestAppLog(TestCase):
    def test(self):
        try:
            asys = ActorSystem(systemBase='multiprocQueueBase')
            applog = asys.createActor(actorClass=LoggingActor)
            recipient = asys.createActor(actorClass=MockActor)
            recipientP = ActorProxy(asys, recipient)

            with self.subTest('Register log recipient'):
                result = asys.ask(actorAddr=applog, msg=MsgSetLogsDestination(destination=recipient))
                self.assertIsInstance(result, Acknowledge)

            with self.subTest('Send log to recipient'):
                asys.tell(actorAddr=applog, msg="TEST")
                time.sleep(1)
                recipientP.receiveMsg_MsgLog.assert_called_once()

        finally:
            asys.shutdown()


class StatusActor(AppStatus):
    def receiveMsg_Status(self, message, _sender):
        self.send_status(message)


class TestAppStatus(TestCase):
    def test(self):
        try:
            asys = ActorSystem(systemBase='multiprocQueueBase')
            appstatus = asys.createActor(actorClass=StatusActor)
            recipient = asys.createActor(actorClass=MockActor)
            recipientP = ActorProxy(asys, recipient)

            with self.subTest('Register status recipient'):
                result = asys.ask(actorAddr=appstatus, msg=MsgSetStatusDestination(destination=recipient))
                self.assertIsInstance(result, Acknowledge)

            with self.subTest('Send status to recipient'):
                asys.tell(actorAddr=appstatus, msg=Status())
                time.sleep(1)
                recipientP.receiveMsg_MsgStatus.assert_called_once()

        finally:
            asys.shutdown()


class TestKlines(TestCase):
    def test(self):
        klines = Klines(
            symbol='BTCUSDT',
            interval='1m',
            start_date=Datetime(year=2021, month=1, day=1, hour=1, minute=0),
            end_date=Datetime(year=2021, month=1, day=1, hour=1, minute=1))
        print(klines.data)
        self.assertEqual(len(klines.data), 2)
        self.assertEqual(klines.data.iloc[0].num_trades, 842)


class APhonebookUser(PhonebookUser):
    def receiveMsg_str(self, message, sender):
        if message == 'register':
            result = yield from self.register(name='bobi', address=self.myAddress)
        else:
            result = yield from self.lookup(name=message)
        self.send(sender, result)


class TestPhonebookUser(TestCase):
    def test(self):
        try:
            asys = ActorSystem(systemBase='multiprocQueueBase')
            phonebook = asys.createActor(ActorPhoneBook, globalName='phonebook')
            bob = asys.createActor(Actor)
            bobi = asys.createActor(APhonebookUser)

            result = asys.ask(actorAddr=phonebook, msg=Register(identifier='bob', address=bob))
            self.assertIsInstance(result, Acknowledge)

            with self.subTest('Register self in phonebook'):
                result = asys.ask(actorAddr=bobi, msg='register')
                self.assertIsInstance(result, Acknowledge)

            with self.subTest('Lookup self registered'):
                result = asys.ask(actorAddr=phonebook, msg=Lookup(identifier='bobi'))
                self.assertIsInstance(result, Response)
                self.assertEqual(result.address, bobi)

            with self.subTest('Lookup nonexisting'):
                result = asys.ask(actorAddr=phonebook, msg=Lookup(identifier='bobibob'))
                self.assertIsInstance(result, Error)

        finally:
            asys.shutdown()


class Supervising(Supervisor):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.timmy = None

    def receiveMsg_str(self, message, sender):
        if message == 'create_child':
            self.timmy = self.create_child('Timmy', Supervised, ['Timmy', 5])
            self.send(sender, self.timmy)
        if type(message) == Config:
            print("Creating Stream")
            self.create_child('Stream', Stream, ['config', message])
            self.send(sender, self.timmy)
        elif message == 'talk_to_child':
            response = yield Ask(recipient=self.timmy, message=ChildMessage())
            self.send(sender, response)

    def receiveMsg_Config(self, message, sender):
        print("Creating Stream")
        self.create_child('Stream', Stream, ['config', message])
        self.send(sender, self.timmy)

    def child_recreated(self, child: Supervisor.Child):
        self.timmy = child.address


class ChildMessage(DialogMessage):
    pass


class TestStream(TestCase):
    def test(self):
        try:
            asys = ActorSystem(systemBase='multiprocQueueBase')
            s = asys.createActor(actorClass=stream.Stream)
            recipient = asys.createActor(actorClass=MockActor)
            recipientP = ActorProxy(asys, recipient)

            with self.subTest('Configuration'):
                config = stream.Config(
                    subscriber=recipient,
                credentials=None,
                channel='kline_1m',
                market='btcusdt')
                asys.tell(s, config)

            time.sleep(10)

            recipientP.receiveMsg_StreamMessage.assert_called()

        finally:
            asys.shutdown()

    def test_exit_request(self):
        api_key = 'r61gnO2tXAkigraBSAsWB6SKxWXHxY0HVD1yxzMbrNSa4XHWyvQTTC00lEbpsp4b'
        api_secret = 'oApCvHN2Yl60D3GOgF6920zAW87KgC8K5NM5evaNV1S7KiBhPaVBJfUzWbzcOLx5'

        try:
            asys = ActorSystem(systemBase='multiprocQueueBase')
            print([p for p in psutil.process_iter() if re.search(r"ThespianQ", p.name())])
            p0 = len([p for p in psutil.process_iter() if re.search(r"ThespianQ", p.name())])

            s = asys.createActor(actorClass=Supervising)
            config = Config(
                subscriber=s,
                exchange="binance.com-testnet",
                channel='!userData',
                market='arr',
                credentials=Credentials(api_key, api_secret)
            )
            asys.tell(s, config)
            time.sleep(2)

            print([p for p in psutil.process_iter() if re.search(r"ThespianQ", p.name())])
            p1 = len([p for p in psutil.process_iter() if re.search(r"ThespianQ", p.name())])
            self.assertEqual(3, p1 - p0)
            asys.tell(s, ActorExitRequest())
            time.sleep(5)
            print([p for p in psutil.process_iter() if re.search(r"ThespianQ", p.name())])
            p2 = len([p for p in psutil.process_iter() if re.search(r"ThespianQ", p.name())])
            self.assertEqual(p0, p2)

        finally:
            asys.shutdown()


@initializing_messages([('name', str), ('age', int)])
class Supervised(DialoguingActor):
    def receiveMsg_ChildMessage(self, message, sender):
        self.reply_to(
            orig_message=message,
            recipient=sender,
            message=Response({'hello': 'Hi, my name is %s and I am %s year old' % (self.name, self.age)}))

class TestSupervisor(TestCase):
    def test_restart(self):
        try:
            asys = ActorSystem(systemBase='multiprocQueueBase')
            s = asys.createActor(actorClass=Supervising)

            with self.subTest('Create child'):
                child = asys.ask(s, 'create_child')
                time.sleep(1)
                print('Talk to Timmy')
                response = asys.ask(s, 'talk_to_child')
                self.assertIsInstance(response, Response)
                response = response.hello
                self.assertEqual(response, 'Hi, my name is Timmy and I am 5 year old')

            with self.subTest('Child exited'):
                asys.tell(child, ActorExitRequest())
                time.sleep(1)

                print('Talk to Timmy again')
                response = asys.ask(s, 'talk_to_child', 5)
                self.assertIsNone(response)

                time.sleep(10)

                print('Talk to Timmy again again')
                response = asys.ask(s, 'talk_to_child')
                self.assertIsInstance(response, Response)
                response = response.hello
                self.assertEqual(response, 'Hi, my name is Timmy and I am 5 year old')

        finally:
            asys.shutdown()

    def test_exit_request(self):
        try:
            asys = ActorSystem(systemBase='multiprocQueueBase')
            p0 = len([p for p in psutil.process_iter() if re.search(r"Thespian", p.name())])
            s = asys.createActor(actorClass=Supervising)

            child = asys.ask(s, 'create_child')
            time.sleep(2)
            print([p for p in psutil.process_iter() if re.search(r"Thespian", p.name())])
            p1 = len([p for p in psutil.process_iter() if re.search(r"Thespian", p.name())])
            self.assertEqual(2, p1 - p0)
            asys.tell(s, ActorExitRequest())
            time.sleep(5)
            p2 = len([p for p in psutil.process_iter() if re.search(r"Thespian", p.name())])
            self.assertEqual(p0, p2)

        finally:
            asys.shutdown()

    def test_actor_system_shutdown(self):
        p0 = len([p for p in psutil.process_iter() if re.search(r"Thespian", p.name())])
        asys = ActorSystem(systemBase='multiprocQueueBase')
        s = asys.createActor(actorClass=Supervising)

        child = asys.ask(s, 'create_child')
        time.sleep(1)
        p1 = len([p for p in psutil.process_iter() if re.search(r"Thespian", p.name())])
        self.assertEqual(4, p1 - p0)
        asys.shutdown()
        time.sleep(1)
        p2 = len([p for p in psutil.process_iter() if re.search(r"Thespian", p.name())])
        self.assertEqual(p0, p2)


class TestTimer(TestCase):
    def test(self):
        try:
            asys = ActorSystem(systemBase='multiprocQueueBase')
            t = asys.createActor(actorClass=Timer)

            start_time = Datetime.now()
            asys.ask(actorAddr=t, msg=MsgTimer(duration=timedelta(seconds=5)))
            end_time = Datetime.now()

            self.assertEqual((end_time-start_time).seconds, 5)

        finally:
            asys.shutdown()

class TestDatetime(TestCase):
    def test(self):
        now = Datetime.now()
        serialized = pickle.dumps(now)
        restored = pickle.loads(serialized)
        self.assertEqual(now, restored)

class TestBot(TestCase):
    def test_append_order(self):
        orders = DataFrame.from_dict({
            'status': ['FILLED', 'FILLED', 'UNFILLED', 'FILLED'],
            'side': ['BUY', 'BUY', 'BUY', 'SELL'],
            'executedQty': [Decimal(1), Decimal(1), Decimal(0.1), Decimal('0.5')],
            'cummulativeQuoteQty': [Decimal(100), Decimal(50), Decimal(5), Decimal(75)],
            'time': [1623502891000, 1623502892000, 1623502892500, 1623502893000]
        })
        orders.set_index('time', drop=False)
        position = position_from_order_list(orders=orders)
        self.assertEqual(Decimal(50), position.price)

        order = {
            'cummulativeQuoteQty': Decimal('225'),
            'executedQty': Decimal('1.5'),
            'side': 'BUY',
            'type': 'MARKET',
            'status': 'FILLED',
            'time': 1623502894000
        }
        new_orders = orders.append(Series(data=order, name=order['time']))
        position2 = position_from_order_list(orders=new_orders)
        self.assertEqual(Decimal(100), position2.price)

class TestMemory(TestCase):
    def test(self):
        try:
            tracemalloc.start()
            asys = ActorSystem(systemBase='multiprocQueueBase')
            t = asys.createActor(actorClass=Timer)

            start_time = Datetime.now()
            asys.ask(actorAddr=t, msg=MsgTimer(duration=timedelta(seconds=5)))
            end_time = Datetime.now()

            snapshot = tracemalloc.take_snapshot()
            top_stats = snapshot.statistics('lineno')

            print("[ Top 10 ]")
            for stat in top_stats[:10]:
                print(stat)

        finally:
            asys.shutdown()
