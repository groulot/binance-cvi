import gc
import sys
import time
import tracemalloc
import unittest
from datetime import timedelta
from operator import itemgetter
from unittest import TestCase, TestLoader, TextTestRunner

from binance import Client
from datetimeutc import Datetime
from pympler.asizeof import asizeof
from thespian.actors import ActorSystem

from b.exchange import Exchange
from b.spot.symbol import Symbol, MsgGetKLines

from pympler import tracker

class TestPandas(TestCase):
    def test_symbol(self):
        try:
            asys = ActorSystem(systemBase='simpleSystemBase')
            s = asys.createActor(actorClass=Symbol)
            asys.tell(s, Client())
            asys.tell(s, 'BTCUSDT')
            asys.tell(s, timedelta(days=1))
            asys.tell(s, Exchange(exchange='binance.com'))

            tr = tracker.SummaryTracker()
            klines = asys.ask(s, MsgGetKLines(
                start_datetime=Datetime(year=2020, month=1, day=1),
                end_datetime=Datetime(year=2020, month=3, day=1),
                columns=['open_time', 'close']
            ))
            print("Size: ", asizeof(klines))
            print("Test diff")
            tr.print_diff()

            del klines
            gc.collect()

            time.sleep(5)
            print("done")


        finally:
            asys.shutdown()


if __name__ == "__main__":
    unittest.main()